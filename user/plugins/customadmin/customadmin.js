l$ = {
  icons: {
    fav: 'favicon-violet.png',
    apple: 'soft.png',
  },
};

document.getElementsByTagName("html")[0].setAttribute("lang", "fr");

var script = document.createElement('script');
  script.src = "https://framasoft.org/nav/nav.js";
  document.getElementsByTagName('head')[0].appendChild(script);

jQuery(document).ready(function () {

  // Grav installé en sous-dossier pour la personnalisation en local
  var gravRoot = location.protocol + '//' + window.location.host;
  gravRoot += (location.href.indexOf('/grav/') > -1) ? '/grav' : '';

  /* Message d’info */
  jQuery('#messages').append(`
    <div class="info alert">
        Framasite est en phase de test. Le service fonctionne,
        mais n’est pas encore facile à utiliser par quiconque.
        C’est à l’écoute de vos retours que nous allons l’améliorer et
        le documenter au cours des semaines à venir.<br>
        Vous pouvez effectuer vos retours sur
        <a href="https://framagit.org/framasoft/Framasite-/framasite-grav/issues/">notre gestionnaire de tickets</a>,
        ou bien en <a href="https://contact.framasoft.org/#framasite">nous contactant directement</a>.
    </div>`);

  /* Message d’avertissement - Bug `common` */
  if (window.location.href.indexOf('/admin/pages/common') > -1) {
    jQuery('#messages').append(`
      <div class="warning alert">
        Lorsque vous modifiez la page <code>common</code>, il est important de
        bien faire attention que les shortcodes soient correctement fermés.<br>
        <strong>S’il manque une balise de fermeture, le site ainsi que
        l’espace admin risquent d’être complètement cassés.</strong>
        C’est un bug critique non résolu mais si ça se produit pour vous,
        <a href="https://contact.framasoft.org/#framasite">contactez-nous</a>
        pour qu’on vous le répare.
      </div>`);
  }

  /* Supprimer ce site (lien vers Frama.site) */
  if (window.location.href.indexOf('/admin/config/') > -1) {
    jQuery('#titlebar .button-bar').prepend(`
      <a href="https://frama.site/" class="button" style="background: #da4b46;">
        <i class="fa fa-trash" aria-hidden="true"></i> Supprimer ce site
      </a>`);
  }

  /* C’est masqué mais c’est aussi désactivé en dur */
  var hiddenFormSections = [
    // Système
    '[name="data[pages][expires]"]',      // En-tête HTTP
    '[id="toggle_cache.enabled1"]',       // Cache
    '[id="toggle_twig.cache1"]',          // Twig
    '[id="toggle_assets.css_pipeline1"]', // Asset
    '[name="data[errors][display]"]',     // Erreurs
    '[id="toggle_debugger.enabled1"]',    // Debug
    '[name="data[session][timeout]"]',    // Session
    '[id="toggle_gpm.releasesstable"]',   // Avancé
  ];
  jQuery(hiddenFormSections.join()).parent().parent().parent().parent().parent().hide();

  var hiddenSettings = [
    '[data-grav-field-name="data[pages][process]"]', // Traitement Md/Twig
    '[data-grav-field-name="data[pages][events]"]',  // Événements Page/Twig
    '[name="data[pages][append_url_extension]"]',    // Ajouter l’extension .html (conflit de template)
    '[data-grav-field-name="data[pages][redirect_default_route]"]',  // Route redirection
    '[name="data[pages][redirect_default_code]"]',
    '[data-grav-field-name="data[pages][redirect_trailing_slash]"]',
    '[data-grav-field-name="data[pages][ignore_hidden]"]',
    '[name="data[pages][ignore_files]"]',
    '[name="data[pages][ignore_folders]"]',
    '[data-grav-field-name="data[pages][twig_first]"]',
    '[data-grav-field-name="data[pages][never_cache_twig]"]',
    '[data-grav-field-name="data[pages][frontmatter][process_twig]"]',
    '[name="data[pages][frontmatter][ignore_fields]"]',
    '[data-grav-field-name="data[languages][override_locale]"]',
    '[name="data[images][cache_perms]"]',
    '[name="data[media][unsupported_inline_types]"]',
    '[name="data[media][allowed_fallback_types]"]',
    '[name="data[pages][theme]"]',

    // Options avancées des pages
    '[data-grav-field-name="data[header][process]"]',
    '[name="data[header][append_url_extension]"]',
    '[data-grav-field-name="data[header][twig_first]"]',
    '[data-grav-field-name="data[header][never_cache_twig]"]',
    '[data-grav-field-name="data[header][debugger]"]',
    '[name="data[header][template]"]',
    '[name="data[header][append_url_extension]"]'
  ];
  if (window.location.href.indexOf('/admin/pages/common')) {
    hiddenSettings.push('[name="data[header][visible]"]');
  }
  jQuery(hiddenSettings.join()).parent('.form-field').hide();
  jQuery(hiddenSettings.join()).parent().parent().parent('.form-field').hide();

  jQuery('[id="tab-content.options.advanced2"] h1:contains("JSComments")').hide();
  jQuery('[id="tab-content.options.advanced2"] h1:contains("JSComments")').next('.form-section').hide();

  var hiddenLinks = [
    '#admin-menu [href="'+ gravRoot +'/admin/themes"]',        // Themes (seulement Gravstrap et pas de paramétrage derrière)
    '#admin-menu [href="'+ gravRoot +'/admin/tools"]',         // Tools (import/export de zip)
    '.form-tabs [href="'+ gravRoot +'/admin/config/media"]',   // onglet Medias
    '.form-tabs [href="'+ gravRoot +'/admin/config/info"]',    // onglet Info
    '#titlebar [data-gpm-checkupdates]',                     // Vérifier les mises à jour
    '#titlebar [href="'+ gravRoot +'/admin/themes/install"]',  // Ajouter thèmes
    '#titlebar [href="'+ gravRoot +'/admin/plugins/install"]', // Ajouter plugins
    '#admin-dashboard #notifications',                       // Notifications (de Grav, en anglais)
    '#admin-dashboard #news-feed',                           // Fil d’actualité (de Grav, en anglais)
    '.gpm-plugins .danger',                                  // Supprimer un plugin
    '#updates .updates-chart',                               // Bloc Maintenance mises à jour
    '[data-delete-url^="'+ gravRoot +'/admin/pages/common/task:delete"]',  // Boutons de suppression des dossiers common et images
    '[data-delete-url^="'+ gravRoot +'/admin/pages/images/task:delete"]'
  ]
  jQuery(hiddenLinks.join()).hide();

  jQuery('.notice.alert [href="https://github.com/getgrav/grav-plugin-admin/issues"]').parent('.notice.alert').hide();
  jQuery('#grav-update-button').parent('.alert.grav').hide();

  var lockedPlugins = [
    '[href^="'+ gravRoot +'/admin/plugins/blog-injector/task"]',  // BlogInjector
    '[data-gpm-plugin="blog-injector"] ~ #blueprints',          // BlogInjector config
    '[href^="'+ gravRoot +'/admin/plugins/gravstrap/task"]',      // Gravstrap
    '[data-gpm-plugin="gravstrap"] ~ #blueprints',              // Gravstrap config
    '[href^="'+ gravRoot +'/admin/plugins/customadmin/task"]',    // Customadmin
  ]
  jQuery(lockedPlugins.join()).hide();

  jQuery('[data-grav-field-name="data[twofa_enabled]"]').parent().parent().hide();

  /** CustomCSS - ThemePicker *********************************** */

  jQuery('ul[data-collection-holder="css_files"]').parent().parent().parent().parent().hide();

  themes = [
  //[ b-menu,    c-menu,    c-h1,      b-body,    c-icon,    c-text,    b-footer,  c-footer,   Nom],
    ['#232323', '#ff9300', '#ff9300', '#131313', '#ff9300', '#d9c4ae', '#232323', '#ff9300', 'Darkly'],
    ['#ffffff', '#563529', '#ffffff', '#ffffff', '#563529', '#444444', '#FFB839', '#563529', 'Hard Sun'],
    ['#000000', '#274F7C', '#ffffff', '#ffffff', '#274F7C', '#202020', '#2A251F', '#274F7C', 'Deep Blue'],
    ['#96ADC7', '#364461', '#ffffff', '#F6F6F6', '#364461', '#6e6e6e', '#96ADC7', '#364461', 'Soft'],
    ['#EEEEEE', '#6A5687', '#ffffff', '#ffffff', '#6A5687', '#333333', '#DFDFDF', '#333333', 'Frama'],
    ['#1C2228', '#FF4081', '#FF4081', '#F9F0F2', '#FF4081', '#1C2228', '#1C2228', '#FF4081', 'Pinky'],
    ['#000000', '#ffffff', '#ffffff', '#232323', '#ffffff', '#ffffff', '#000000', '#ffffff', 'Black Impact'],
    ['#DABE27', '#977700', '#b3e27a', '#B3E27A', '#977700', '#062e00', '#51C71C', '#977700', 'Green Wash'],
    ['#3B3B3B', '#44685E', '#ffffff', '#ffffff', '#44685E', '#2A3A42', '#96C7A2', '#44685E', 'Grav'],
    ['#141414', '#0300FF', '#ffffff', '#1D1D1D', '#0300FF', '#5753a8', '#111111', '#0300FF', 'Electric'],
    ['#410000', '#FFA203', '#f2ac8b', '#131313', '#FFA203', '#edd17c', '#1F1717', '#FFA203', 'Aphrodite'],
    ['#A46213', '#FF8A00', '#242424', '#f3eadc', '#FF8A00', '#4f3114', '#8A5728', '#FF8A00', 'Tasty'],
  ];

    themesList = function themesList(data) {
      let html = '';
      for (let i = 0; i < data.length; i += 1) {
        html = `${html}
          <li class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <p style="background:${data[i][0]};color:${data[i][1]};">Menu</p>
                <p class="mini-jumbo" style="color:${data[i][2]};">Lorem ipsum</p>
                <p class="mini-body" style="background:${data[i][3]};color:${data[i][5]};">
                  <i class="fa fa-gear" style="color:${data[i][4]};" aria-hidden="true"></i>
                  A adipisci aliquid amet ipsi<br>aspernatur cumque
                </p>
                <p class="foot" style="background:${data[i][6]};color:${data[i][7]};">amet aspernatur</p>
              </div>
              <div class="panel-footer">
                <a href="#" onclick="themeCSS(['${data[i].join('\', \'')}']); return false;">
                  <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                  <span class="sr-only">Utiliser</span>
                </a>
                ${data[i][8]}
              </div>
            </div>
          </li>`;
      }
      return html;
    }

    themeCSS = function themeCSS(data) {
      const style = `
body {
  background-color: ${data[3]};
  color: ${data[5]};
}

.module p {
  color: ${data[5]};
}

.module .item-icon {
  color: ${data[4]};
}

a {
  color: ${data[4]};
  opacity: 0.85;
}

a:hover {
  color: ${data[4]};
  opacity: 1;
}

#header .navbar,
.navbar.scrolled {
  background-color: ${data[0]};
  width: 100%;
}

#header a,
.navbar.scrolled #navbar1 a,
.navbar.scrolled .navbar-brand {
  color: ${data[1]};
}

#header .navbar-toggle {
  border-color: ${data[1]};
}
#header .navbar-toggle .icon-bar {
  background-color: ${data[1]};
}

.jumbotron h1,
.jumbotron p{
  color: ${data[2]};
}

#body h2 a {
   color: ${data[4]};
}

.tags a.btn {
  background-color: ${data[4]};
  border: none;
}

#body .col-md-9 {
  background: ${data[3]};
  padding: 0px 30px 15px;
  margin-top: 15px;
}

#body .col-md-9 + .col-md-3 {
  padding: 15px 0px 0 30px;
}

#contact {
  background: ${data[3]};
  border: none;
}

footer.footer {
  background: ${data[6]};
  color: ${data[7]};
}
footer.footer a {
  color: ${data[7]};
}`;
    jQuery('textarea[name="data[css_inline]"]').val(style);
    alert(`Thème « ${data[8]} » importé.
N’oubliez pas d’enregistrer les changements.`);
  }

  jQuery('textarea[name="data[css_inline]"]')
    .height('600px')
    .parent().after(`
      <style>
        #themePicker .panel-body { padding: 0; font-size: smaller; }
        #themePicker .panel-body p { margin: 0; padding: 0 10px; }
        #themePicker .panel-body .mini-jumbo { font-weight:bold; padding: 20px; text-align: center; background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAABQCAYAAAAnSfh8AAAgAElEQVR4nIzc1Xfj+53u+V+BmWXJki3bIltmZigzSraYmRnNVFybsgM7yQ7snXQ66T7dPQ0502fWmjNX87e950Iu7VQn65y5eFbZdVGrZPul5/OFn4XNfQmzy/WsbHZicowyOdfE3pGGo5MR5pelaEeamVvsJhzVs/ZikLUXQzyvFRCJBVpFAp29AhKNgGq2hmWjlE1nN3OnzYxuPaVrRGBgqgZpn4BCXcPamoLFJRna4WfMLDWxdSRhYaeRY6+CHZsMe3aC6f1GloydnCSGMKRGOE0Oo3khoF4WGFgW0K4ILOzXsrT7jJX9GubWBEZmBGaWBI6NbTj9fZhdEvSWVgz2Dk6d7djD3fiSamyxPvbsbSzpn7Ogq2HF0MCuu4PjoBRjVI453oc7qyJ6OUbuborCwwylh2mK91OUb6cp305zcTfL1cM8t69XuHu7wfnLLa7e7lO+3+Dus13SpQm8kX6KF7MEohrs3j6sXgV6q4wTpxxzUIUtOYgjPYS3OIw3ocId7cURkuMM9+IMKXCFlfijQ4STY4STI8QyoyRy46SKY+TOxilejpE/HyFdUpM5H6BwPczFwxQ3bxa4fb3E1cM8patxcudakgUV8Vw/oXQPvqSMQLqPYFaFP6PGl1ISzw+QyKlJ5LTEMgOEEwP4I0qcvl6sLlk1ZpcEs0v8GBFmdztmdzsWXycWXydWvxhbQIItIMXq78Lk6eTU2c6xtZVDczP7xkb2jY3snjSwdVzL1nEt2/pa9gx17J4+Z/f0OXuGGg5MDRxZmtDZWtDZWtDbWzF5OnGGe/AlFATTaqJ5LenzcQo3MxSu58lfzZG9mCF9NkWyNEWqPE32Yo781QKl20WKNwsUb+cp3s5TupujdDdH8Xaawu0EqYthoiUVgawcX7obb1KCI9KONdiCJdCMM9SGM9SGK/gxHY8R4QqK8EQkeKNd+GLdBBJyQikF4bSSSFpDJK3BF+nFG5bjCcnwhGR4w1L80W78MSm+mBRfoo9ARkmkMEC0OEikMEA4r6l+Hs5riBYHiZeHSJwNkzwfIX05RvZ6gvztFIJ6WODgpB9XYBaXf57ylYGXb32MTjYxuyhharaTsckOunufMDXTzfRsL82tAm0dAqIugTaZQKdKQD4uMHXQyrZbzkFIwaZdinpBoEUmMDLVgrz/CRKJwOhYMyNjtQxPPmXvpJsXOhGrx60ce1Vs2brZcfaybpOxauniMKzGUZphVteMYkGgf1ZgdFVgbvsZS7s1bBw1sLZfz/SywPyqwMFJM2anFINdxIm1DYtbgsUnwRXrJZAewJFQcOTuZM1Yz/JJHcun9WzYmtnziDgJdWOMynGk+gmdDZO5maDwMEPxforC3SSlmwri89sZLu/nuHm1zM3rNV59eczl221u3u1y+eoFt683ubxf4+HNLun8LMnMHFaXGm9kHKNHiTs5ij0+gCHShzXWhzumwBGSYw/24AhWALsjGoLxEaLpCeLZKZL5KVKFSdKlcXJn4+TORkkXB4nlFCRLCrIXGsq341y+nOX65RyX93OUrsbJXwyRKqqrgIPJnirgQHoAf0pDJKMimlYRywwQTQ0STgzgC6tw+nqxubsx22WYHV2YHF0YHZ2YnJ2YXSIs7k4sno7/LWCdre1vAt48qmFbX8vuaW0V776xlkNzI0eWJo6tzRxbm9HbWzF7xbgicnwJBYGUikhukNTZGIWbGYq38xSu58ldzpI5nyZ9NkPmfJbc5TzFmyVKt4sVxLfzlO4WKN/PVwHnb8ZJng8RKSo/AeyMdmALtWILteKOdOCOdOAJf0znY8R4wuJHvFL88R6Cyd6/AhyI9eOL9FYBf0TsDUvxRKR4YnJ8qX5COTXhvIZQTk0wqyKYVVU/jxQGiJW0VcSpi1EyV+PkbiYRJucaWd3s4vBEw/xKB9cPNiLJTXSGEcammhkeb2J9U4nRssTewSTrG8NY7Zs0Ngs8rRFoEgt0KAR6JwQm91tYNolYtXWyYetiaqcJ7Vw9M8tdTM/JWFiQs72jYXVdysRcHbOrDcxvN7G434zeN8gLkwx9aIhVSxdLxk6s2SniL7cxxofQrAh0TwhMvBCY3hBY2H7Gtr6VQ7OYjaMmtnVNGF1SHMF+LF4ZZm8XznBP5RufUhLKDeBOKzGGutlxtLJmrGfltJYX5mb2XB3ogzKMUTm2RC/evIbExQjZ20kKd5Pkbyco3UxQvB7n7OaHFr5+vcj56yUu3q5x//k2pdtl8udLvHl/wjffBPiHP53zT/94x9n5ManCBptHnVgDajyZERwZNda4HEekB1ugG5u/B0ewH1dYjTc6RDA5RjQ7RTI/Q7o4Q7Y8Q/Zsitz5FLmLMVKlIaLZfuKFPhJnSgrXw5zdTXB5P8P57RSlq3EKl8OkSxoSeQXhjLwKOJBREkgPEEhpCMQVhBJKIik1keQAwZgab0iJ09uPzSXHbO/BZJNisHVhsIkx2EWPiMVY3J2YvSLMXhEWnwSrv+sTwAZXB3p7O0eWlr8JeEtXw+5p7V/grefI0vRJ9PZWLD4J7mgv/qSSQEpFODtAsjxK/nq62qwfWzhzPk32Yob81dwPeP+igYu3sxRvZ8lfT5K9GiVxpiVcUPwVYHu4DUekHU9UhCcqwhv5GPFjJHgjEvzxHvzxHgKJPoLJfkJJFeGUuppQQkkg1o8n1IM7KMUV6MIdlOAOSnCFJLijPfgSfQTTSoJpJYGUAl+iD3+yn0BKQSijIpxVE80PECsMkigNkTobIXMxRu5qAqFPJbC8LkVnGOHixsr8SgeaYQG19gm9SoF2sYCsV6CrW0AsFVCqG1GomqlvEOjuraOjR6C1V6B7VGDuqJ05fQtDWwLTB/UsHHXQOyIwuyLF7nqBy7XBsW6M49NR9vQqljbbmVmrY/Oki3WdhC1zD1tWOfPH7ew4+7GlJ8m83iP9cofZgyZ6JwWmN54xsy6wtF3Dlq6VE0c3ersMq7+faG6MZGmCQEqDKyLHE+vDG+/Hn1YRzGrwZ9TY4/3ofRJ2rC2sGevZtLZy6O3CEOnDHO/HFu/HlVESKQ2SuhqrAs7fjFO8Hqd8PcH57RRXD/NcvZrj4u0s91+ucPawwNsvDrl92OezDw6uLw28uffy06+yfP1lmt98d0a2vIs7MoYx0Icro8SV6sMekmPz92Dz9+IMqfBEtPhiw4RSE8TzsyTzM2TLcxQuFilezVG6nqN0PUP+YpJ0WUu8rCB1oaRwraV8N8bF/STnt1OUrycoXo2QPRuoAg6l5ITT/QRSKgLJAQLJAfwxJaG4mnBCQzgxQCCqwRNQ4fQqsbsUWB19WBxyjPbuKuJKEz+O017xI2JxFbHFJ/mbgPcMDezo69k8qvmkgT/iPbY2cmxt5sjSxKG5kUNzI6fOdmwBKd744/87pSKU0VQBF27myF/PkrucrQLOnE+Tv5qjeLNQHbHz17Pkr2cp3MyQv54mczFG6nyIeHmwCtif6cGX6sId78QVE+GOd+KLi/HFxfhjH9P1GCn+mJRQorfavKGUglBS9WkSSvzRPtzBbpx+6WMkuAJdOINiXJFuvPFe/Ml+/Ml+vPFe3NEe3NEevPFeAikFwbSScFZNJKepIq4C3j8aoV/1lKk5EZ7AGkcnQyyvS+mUCfSrBbSjNSysdLF3OMzuwQhO9za7+9OIJc+obRCQKupokQuINAKTOy0snHYwcVDD3HETcwdtzG6KUQ3XMjzewca2FqNlAZ1hjLnVdhZetGLyDLNv6WNDJ2PjRMqqXsKuQ4ElPslpaBhvYZHsy122LDK0CwKzm3UsbNWyttfEtq6DI6uUU7ccb3KIzOUc+Zt5YsVRfCklnkQvvlQ/vlR/pXWyGnxpDdZoL0dOEVvmJjatrRx4JBgifVgSCuwJBc60glBBQ/JytIL3doL8zSj5m1FKN2Oc3U1w9TDL1etprt9P8PLrRcoP07z/6pi3b43kUrukI0fEfMcE7Hu8u0/xu9888P3vL7l6qcPiU+CIynEne3FG+rCH+rGH+nFFNPhiwwQSo0QyUyQKcySLc+QuFzm7WeX8bonzu0Uu7hc4u52jeD1O5mqA7O0AhbthSvfjnN1NUL4dp3wzRul6lOzZAPFcL+FUD+F0L+G0En9SiS+uxp/QEIirCSUHCCeHCMaH8EW0uPwaHF4Vdo8ap28Ah1eD1aXE7OjF5OjBaJdW4ujE6BZjdIsweTofMXdh9nZhdIs5dYp+AGxoYu+08a8A7xnqODA1VNe7OlvLJw1sdLdjD3ZV1oopRbWVEqUhspfj5K6mHjND5mKK9NkUmfNpcpez1dE6dzlL7mrmEfE0uasJ0uejJM+0xEoDhAsKQtleQtleAhkZ3qQEf0JCMCUlkOwikOwimPgYGcGEjHC8m2Cim0iqn0jqcYJJfXwj1BCKqwnGVPhjSjzhPhz+bmxeCTavBEdAgiskrSTSXRmjE3144724It04QlKcYdknuD++7khOQ7yoJXU2QvZyHGFuUU6HWKBf9ZydfS2H+mFUgwIqrcDUfDPa0Ro6JAKqweeMjLcxO9/L9GwfT54K9CqaaZcJdKme0Dv2lOH1OiZ26pg+rmfVKGbuoI3RpWZ6NU9YXFWwtTvM1KyE4fEmJhea0Vu0uCNzLG+LMHvGmNtsYXGvA513EFN4DFt8ikOXGndqhiOnkoWdRha3mljebmR9t43Now72TWKOnd3YYxpipUnSVzMkzkaJFLSE8xr8aUUVcTCrIpAdxJ1QYvRJ2bd1sGFoYdsm4tjXgymmwJ5Q4kqpCOUHSVyMkLsZr+R6hNz1yA+AX05z/WaK8zfDXL+f4P79Mrcvt3j32oLXtsSrswi3uShRh4HzdJCroo+///1Lfv2bLJHMDK6oCk9ciTumxhlR4gyr8cQGCSRGCaXGieVmSJXmyV0sU7pd5fJ+jcuHFS7ul7l8WObifoHS/RTF+xGKLyt4S/fjlG/HKT3iLV4NkylriGXlhFM9RDN9RDIqAnEVnqgSb0xDMD5IKKEllBgmEBvGExzE5R/E6RvA5R/CFx7DGxrFHRh+hNyP2SmvQja4Kk1rcHVgdIsxeSSYPBIMrs4q4ENzM3unlfF5R19f3cTaPa1n39hYHZVPHG3o7a3V9a/O1oLZK8Ie7MITk3/ygxwrDJIsD5O5mCB7OVnBez5J+qyC+OMYnb2YIXs5/Qh4mvz15CeAo0UN4YKCcK6PcK6PYLYbf1pKKC0jku0hlJYRSssIpz6mp/K1TMqJJvuIZzXEMurKHkJaQyQ5UMUbiCrxRvpxBnqweaVY3BKsHjEOvxR3WIYn0l0F/LF5HSHp416CBHuwC1eku9rGH0ftSE5DojRE+nwUQaluoaVNYHhUhHakGaN1lqGxej58GeXyzsLDWzc/+2UBT+AFK+t9yHqe0tNbh1LVhkjylBaRQPfAc1TTdQyt1TO0/oTx/edsWGWsGaR0awU0o/Vs7AwxPdvD+JQYg2UGq3uOE/souzol4wv17J4oGF+uZ3q9mR1THy/0MqzhcQ5sCg5tCgxeLcfWftZ221lYr2dmuYaljUbWdW1smzs58fXhSg4QLIwQKz/mbIhgVoE/3Yc/rSCYVRHKavEl1Vj83egcYjYMLWxa2jl2yzBH+nEmVLhSKgJZDbGzITJXo5VcDpG9GqZ4M8LZ/TiXr6a4fjPF/RczXL6d4P7dOq9eH/DhtZPLrIWHQpi41UjUfEo+4OQ66+X7b+/4j3/7wNXNMd7oKL7ECJ7YIK6IBndUiz85Sjg9UVn7FufIni+Rv1yhfLfG+cMaZ/fLnN8tcXG/xMX9AuWHacoPE5RejlF8GKd4VxnzC1djFK9GKFwOkS6piWYqP3TxnIJoVl1phZASb1iDL67Fn/jL9h3E6dPi9o/iC00Qis0QjE7jD09WIPuGcLjVWB0KTI4eTh2dnDg6OHF0cOroxOAUY3CKK39vF6GztnNg/GvA27o69gwNHJgqG1YnjjZOne3VFtbbmzl1tmL2irAFJDjDsmpTfRwpo/kBEqUhkuVhkuVREqUR4sVREqUxUmcTpM8nSZ1NVD/OXk6SvRwnczFGsjxMvDRArKghWlASyfcTLSgI5+UEs91Esj0kiv3EsvJPksj0PaafREZJIjdAIqclnh0klhmoAg5EVfgjStyhPhz+HixuCSZnJxZ3J3ZfF+5QD95IbxWoJybHGZZhC0j+Yl+hswrZGZZVIQfTyuprFxqbBPr6m1FpWtjY1rKyrsDpXWXlhYx+jcDIRD3j003MLIgZm2xDM9hCv7KJHnkDfcoWJD3P6egV6FQLaJdrmTloYtHQzoFXyb5LxcRqG0OTLai1LYxNSLE519GbpplaEhHJbHNwOsCeXsXEYhMbhz1s6/oweEZZPejC4BnG7B9jWy/FHhzDFZpg61DM1OIzhkYFxucqI/XyYTO7VjHGYD/ulIZgcZhoaZhIaZBQTkkwqyCQURLKqauAbUE5eqeEbXM72zYxx24ZlqgCZ0KDK1U5ZgkXB0hdDJO6GCZ9oSVzOUTheriy1nw5ydXrSS7fTnH/fpGHt5u8eqXD75zjNu/kPOqg4LVTDrjIOE1k/KfcFd383//5C777TZni2SGe8CTe6DDuqBZPbIhAqrJxFc/PkC4vkL9cIXu+RPFmhfLdCqXbZco3C5zfLXL5sMjly3nKD5MUHyYqa/WbcQpXE+QvxylcjFK4HCZVVBFJd1cBx3Ia/DEl7qACd1CFNzaINzaIPzqEJ1hpXqd3GE9gnEBkmnB8jnB8jlBslkBkCl9gHJdXi92lweLsw2ATo7d1oLd1cGIXVRGf2EXobR0cW9o4MDaze9JQBbytq2NHX2nfQ3OlaU+d7VXAlTZuwehu/+QH+eNI+XFd+HGkrGSQSG6QcFZLJDdErFDB/PHPRGmE1NlYpXnLw8SLWiJ5FbGiimhBSbSgIFZUEin0Esr1EMv3kiwpSOT7Pkkqp3iMklROTTI/SDI/VD2G+98BNrtE2H1duILdeMLyKsyP7WvxdWLydGDydFQhf9zhdzyO3L5EH+GsmlhhEEHW3cDkVB9G8zpHulkkMgG9YQrN0HOUAwITM61MzbUzsyBmbUPF6voA6oE22jsEpD11SHvraZUJ9I7W0j8tML5dz663jxcWKUtHYnq0AoPjTWhHO1ComnixPcyLnQEcgRX01nHGZppY3uxidVuGzjyEL7pEKL3K+n4Xm8dSjixKzO5h9k970ZmU2DyjHJz00q8SGBwTGFt+yvxuI1smETqPHHtCTaA4TORshFBpkGBeiTfdizsuxx2X40uq8cSV2ANyDG4phw4JR65uTnzyxwbW4M0OEC5oiZa1JM4qSZ0PkrkcIn8zQumxga9eT3P7YZnbN6s8vNnn1cMJbtMULwtOCj4DabuOuPmAvNtAyLhJNqDnxx/y/M///DW/+/6BSGwTd2icaHYaf3wUd1hLojBH5myJTHGBs5t1SlernN2scnFXSflmgeLVLOWrOc5u58hcTFC4m+Xy9TLp80luXq+RLo2SyA2QKgyQLCiJZnoJp+SEEr0E4gq8EQWOoBJHQI07oqkkPIgrNIgzqMUVHMMTnsQXmSWaWCISXyQYmcMbnMDrH8MbGMHtG8LuUWK0Szl1SKoxOLs4dUjQ2zrRWUUcmlrYO21kW1f3SXZPKu17ZGmpHhdV0oze3syJo4VTZ2sVsS3YiT0kxhnpwhPvJpDuI5JXEUj34UvK/2KNrKruVocyFdjRvJZYYYhoXlsFHy2oiRU1xEvqx6hIlNWkyspPkiz0kymrKJxrKJxryJU0pPPKKuBMXks6N/RJAwdjKryhflx+OQ5/D2aXhFNbOyfWNkzODuy+LhyhbhyhbpxhWTX2YBdWf2VT0OBq49TZis7W9FdvZo6QtIpYaGsXmF/QsLKmZWlFw/BYO0OjLaSyJ2ztaRgYrmNkopl+1VPaRAL9ynrWXgyxsKRGIqtBJH1Ga5dAz+AzNPN1TO+3sO2Qs2WVs3wsQawU0E40MzgiQtz1hOn5PpbXVRwaxplaamdjr7+aQ8Mgdt8sruAcnug8J3YtFs8IFs8IBtsArsAkgdgidu8EkzPPGZl6wtjyM+Z3m9kyiquA/YUhwuVhgsWB/wK4D29ChTehxhlSYPLIHwFLOfHJMYX7sMcUuNNqgrkBwsUBEmdakudDpC+0ZK+Gq4AvXk5y8XqGm3drXL/Z5OH1EeelXRL+DR7yDnLuIzK2AyInG+ScB5QDp+QDetI+Pf/8dz/mf/7nH3n7OkEouog3Mk4wMU2isECisEAyN0+uvEL5aoPC+QrnNxtc3m9Svl6heLnE2fUSF7crXNwucftum8tXLyjfrZEqz3J+u0bhYp5MsXLklS5piGX7CSa68cW6cQW7sXhlGF3dGF3dOEMqXGE1rtAAzuBAFbA3MoU/Okc0sUQ0sUQoOo8vOI3XP44vMI7HN4rdo8Zo7+bELubELv4E8IldjM4qqrbvx6Oj6gaWrq7awP8rwAZXGyZPBxZ/B9aACHtIjCsqxZeUE8z040/14k304E30VDeC3NFKvPH+KuZgWv2YynFNOKckWlCTPBt4jOZvAk6XlGTP1BQvBqqAMwVVFXA6N0gyM1hdA4cTmsra9xGwzSvD5BRzYm1Db2nF6GjH5pVgD8r+CvDH9e9HwCeOFo6tjehsTdUlhdHdji0gwROTE0gpKiO003VAv7KJYFhPY7OAQl2HwTzP/FIPoegBI+NtNLcJtHcKNDYLPK8VaGoRaGkX6JTWIup5RvfAU0bXWpk9bGNB18bqqYS5vXY6FQKi7spxVGuHwNCYGIWmHu1EM/3aJ5gcs+hMoxycaDE5JzE6xvHHlrl/byOWWyOUWMTsHMJgG8ATmsYVmMbhm2ZlvYPR6eeMzD1har2OlaM2DhwyLBEVnuwg3rwGV6Yff0GJO9OLM96DIyrHE1fiiatwhZVY/f3onVJOvD0Y/L2YQ33Yov04k0r8GTXBvJp4ebCC+GKQ9NVQZSf6foKzl1Ocv5rl4uUqV692uX+pJ+Sf56Zo4jZtIWXdImXepODcx707Scy4ysuch6uEk1999ZL//Jc/8h///GvOL034QzPEUytc3R1RutghX9zg/HKX0tkWhfI651fbXFxvUTpfpVBe5vxilYvLNUoXK+TKKxQvNri82+PV+1PefWbgzYdjEtkx4pkhErkBwik5vljl4oA7LMMerBxb2QIKrP5+bAEFtoAKW0CNzT+AIzhaARybIRxbIBxbqDRwYAqvfxyvfwyXZxibW8WpVYreJkFvk3Bi7+LUIeXUIa028L6hhW1dAxuHtbw4qOHFwTNeHDxj67i2ugb+ePPqvwI+cbRw4mzi1NWM0dOKyduGxd+GPSTCFZXgTchwRSU4wp04wp04I13VJrMFpI9AKncB3NHex2PFyggezimJFTWkzgcr09X5AOkzDekzFekzFZmyqoo3d66hdDlI6XKQfHmAbFFNJqcinVWTzGhIpNXEkmqiCVVl5zmiwBPsxenrxuKWYLB3oDO3oDO3cOJow+z9i5E49MOGljsswxnswuoVYXS2cmpvRmdp4MhUx6GxlmNzPQZHC1avCFdIijfag2BzbOPxHTI+2Y16oJW6BgFp9xPmFvqYX+zH7d1FM9hCR6eArOcpjc0Cz2oEWtsrIDvEzxD11CBVP2ViQ8TMXgsjG89YPO5gfr+D3mGBZolAU5tAZ9cThse6kPU/R6GtY2imFYN9DpNjFqN9Cot7mkPDAMHkMuVbHanSJus7ImaX69nXy4mmV0gXd7l5ZSGd3cXhm2ZmtYmZF00s7rawbZJgCChwptS4MkpsiR78eTWutBx7VIYt3I0z0o8rqsIZUmELKDB65RgDckzBXswhOeZwD9ZoD+5kL76MgkhBTbSoIX6mIXWpJXc9QvFunPLDNOcv5zl7ucbNm33uX59iPNXy9ZsYdxkrScsmCeMLYvolIsdzhPWL5L2H3KY9fPfj9/zT97/mf/zbH/n2l5dc3ZjJZvcplY8pn+konx1xcXlM+eyA0tku5fM9ri73ub464Opqj4uLHS4utri62qNQ2ubtBye/+W2Of//zK/7pX674tz/fcP96j2xpmnBShT/eU8Ebq6y3nJE+HGEFzogSo0eGyduNySPH5OnD7FFh82txh8bxRafxh2YIhGfxBadx+8dwe0dxe0dwuLSYHUpOLF0cWzrRWcVVxHqbBJ1VxJG5nb3TZraO61nff87a3jPW9p6wvv+UzaOa6i70obnxrwB/RKyzN6B3ND5CbsToacbib8MW7MAR7sTsa8XoacbkbcHib8MaEFXPpT+eSVv9XdiDMpzhylozkFIQLahJlAdJX2gr+xvnA49RV1KuIC5cDlK80nJ2PUT5SkvxXEuupCGbV5POKklklMRTKqIJFeGY4hO8NncXRkflVuCxqZljU+V1mjwdnwD2RLrxRnvwRntwh2XYfJ0Yna2c2Jo4NtdzYKhhV/+U/dPn6K2NmN3t2P1inMEuhJk5OVs7o1xeBxF3CUzP9tLb38DQiJj1jREUqmaaWyuN29EpIJEKaIfbGR0X0y4WqG8SaOoU6OgVGF1rY2KrgYEVgWV9Jy8MPcgGBDp6BDokAs1tAj199fSq69GMNjG2IGLrcJADwwju4Aq7OhU68xCOwBRm5wi+yCzx3ArHpj429sRsHXSxti3BFZglktwgkd3h1DHCrlHB+rGYrVMJOk8v9oQaV24Ad0GFr6DGme7DHJZhDMow++VYg5XGsfqVmHy9mIKVGAPdmPxSTH4JtogUV0JOINNPKKckWtSQONOSvRqleDdJ+X6Ws4cFzl+tcv/ZMS/fGjk+VPKrr4q8KwfJ2Q9JGTbx78wQ1S0SPV3Ef7RAzLbPzz/c8/uf/4R/+O4X/POffsLvvn/g83dxri/s3N+4eXnn5uWdk8/eBbm7sXB5fsr1+Smv7+28vDNzUT7irLjH9eUJl2dGPk5o5jYAACAASURBVP88xne/PucPv7/ipz+O8t/+8ZLr612iyVGiqUECiT68URmuqBRbqAuDV4LOKULnEKNziNE7JeidUvTObk6dckxuJQ7/CO7QON7gBL7QJN7gBC7fKC7PSKV9HQMYbb3ojBIOjZ0cGjs5Mok5tnRybOnk0NTBgbGdHX0jG4e1j3h/yMbhc3ZOKjvRh+bGvxijm9HZmqo5stZxZK3j2FaL3lH/iLgJk7cZs6+FU1cDJ856TpwNGD2Vpja4WzC42jC62zG4OjB5Ktc97cGuKuBK+w6RPddWcjH4GA3ZCw2580rOb4Y/ydnlMMWzwSrgVEZFIqUgEu8nGJHjC/bg9kmxujsxOdo5sbagMzdxYGrgwNTAsbUZg6sNa1CCM9KFOyzDG+3BH+/FH+/FE+nG7hdjcrVVG/jAUMOO7gl7J8/QWRowudqwekVYvSIEt28biUxgbUNDQ5OAx3dIXb1AW7vAzu4cI6M99CtakHQ9p1MiIO4S6JY/o1fxnJ7eWlraBRraBUR9AoOLzYys1TG4KjB30MraiYwebeW6ZUOLQEOzgEgiIJIKSPoFxhfFbOxr2D4eIJTYYfGFGE94CWdwGoNNSzS9QiS9iMM/itE+iNk5hMkxSjy7STyzTe78mEh6E3tghmOril1TDwf2HozBfpzZAYIXIwRKWlyZfsxhGSc+CQaPDJOvH4tPidmvwOCvwDUGujn1d3HqEWHwdmIJSXDEuvGn+whmK02cONOSvRyncDNF+X6W8ssFig+L3L7f4+6VnoO9Pn72WY4vr5OU3UaiRy+IHq5gWR4idDhH4Hge9/EKv/j8ju9++iUvL/L88utX/Pn/+Jb//q+/4o+/+4w//u4zfv/b1/zmlzf88fev+e0vr/jm6zw//SrLN19n+frzBG8e3Nxf23hz7+XnX5f5zS/v+MP3r/nlN2Ve3bv4/fdlbq4PSWZmiKa0BOIKXJFubCExRp+YQ2c7u5YWdsytHNraKrF2cGARcWSRcOLowexSY/cN4/KNVprXP4bTO4zTPYTTM4jVoeLU0sPhiYj9045qDozt7Bva2D1pYUffzOZRHev7z1ndfcrq7tNPAFcucjR80sA6W9PjjaxKjqx1HFpqObLWcGyr5cRZz6mrAYO7kVNXwyeADe4mDO4W9I4f1o0njjYMro7qebI72kMwrfwrwLlLLfmrIQrXWoo3Q5SuhyjfDHN1P8bV/RiXd6Nc3I5wfjVC6VxLrqAhnVWSTCuJJ/sJRXvxBWW4fVIcbjEmRzsGWys6cxNHxgb2jfXsG2s5sjRgcLdgDXbijEqreAOJvsc32h4cAQlmdzsGRws6SwOHxlp29U/ZO3lWHaNNrjZMrjaE7b0htna1tIkEumSVpu3ta6K2TkDW3cTC4jBb23MMj3TT/NjCvf21KNR1jIx1IutpprldQKJ8imq2nuHVWkY3axnbqGV6u4WhuabqCC2R1SBXNiLufkpHj8Dseg96yzTH5nFObFPMrYkw2MdwhWYwOYZJFzdxBcdxBsY4PO1j97gbvXkQb3gBs2uSSHqzAj60gM0/idGnRefuR+/twZpUVgCfD+PKqzFH5Oi8EnTuLvRuOUZPP2afEoO/B1OwEkNAisHbiclfwetN9xPMKh4beJDk+QiZq3Hyt1OVp1oe5sndTXH1foOL+30O9/v5cBvlZy/LXPldBHY2cK3PYFsdI3K0hHF1ENPmBL//2Wt+86O3BOwnvLrM8ftvv+J//Osf+O///B3/9Hff8Kfvf8Qfv/uCP373OX/67gN/+PVrvvv5Hd/+5Jyf/6jEN1/l+earPD/9ssDvf/WOb39yxx+//8Bvf3HHF+9i/OH7S26uj0nnFoimhvHHlNiDXZj9HZy4OzhwtLFva2Pf1sG+pYU9czN7plZ2jW3sGTo4NEs4sfZhcqqwuAexeYdweIawuQaxOQawOlQYLX3ojFIO9B3s6tvY0bWyq2+rfrx13MjGYT1rezUsbz9leVtgZedJFfGLg2ds6Wr+aoTW2Zo4sjRwaK6v3I1+BHxoef7YwnVVtHpHHSeuWvTOGk6c9RjclfWy3tFY3fz5uPFj9Ys/OYJJlAfJXI6Quxgid1E5HizejFC+G+H8YYzLh3GuXk5w+2qS64fxKuLLmzHKF0PkiwOks0riyV6i8R4CERneoASnpxObsx2jvY1TawtHxoZHwLV/E/DHO9Qf44vJq+tgk6utugbeP33OgaGGI1MdJ7YmTu3NGBwtCLsHY0xMS6hvFOhTNCDvbaSpWaC94xkNTQJabQ+DWhn1DQLPnguoB1pRDTTQ1S0wOi5G3tdGXatAZ79A/8RzRtcbWTjqYPRFLePrTSjHa+noEWiXCLSJBaS9tfRrmunqExiabMEZWMfiWWB0tomZlXaGp2rQW7Rs7nfjCkxjcgzjCkxycNLP5r6UE4sWs3OM1c0ujk1ajk1DWD3TBJLLhDKLOKJjGAJKbHEVgbMxQhfjuPODmGK96LwSjtwSjt0yTr19GP0KjAE55pAcS0SONSTDEpRiD0vxpfqJFNRE8qof1kvno+SuJh6fgpmtPNVyP8Pthy2uHw441Q/w+iLArz674SEexL+3iXNzAf28ltPlIY4W1Vh3prnN+bhIBng4S/PmssiH+wu++eIVP35/x91ZktfXOX784ZrPXxe5zHm5PwtzWw5QTju4Knj5/FWan3x2xldvC3z/y/d89faC3337ge9/+Yaff33OH//ugbsbI/niC+LZCbxxJZaAmFN3G0f2FnbMzeyY29mztrNnbmbX1MiOsZGt03p2TpvYM7RxbJZwau/B4OjD5FJgcikwO/ox2fsw2no5Mcp+wKtvZvu4pZqto2Ze7DewtlvH8tZzFjcFFjd/ALy6+7SyDj5+zr6xAvXj5Y2/BHxgrvmbgD/i1dlr0dlrObbVoHfUVRv4xNlUXUN/fOTREZLijfdWb3GlL4bJXY+Ruxgif/kRb+V8//rVJLdvZnl4O8PdqxluXk5wfT/J1d0ElzdjnF0OVwHHEnLCURmBkBS3vxOXuwLYbKsAPjY2cGio5/BvAHbFZJUHIB7vUX8E7ApJsXorj2x+BHxgqKkC1lkaOLE1cWJrQpBJn3F0OMf4ZDf9ihbEXfV0dNbQLqqhtlGgoUng6XOBZ7WVXWdZbw3KgQbGp8QsrqroU4lo7HxKe98zmnsEBubrmN0TsXjUxfiLDuQjtTx5XCc3tgm0iAQ0Q60MDNUzMdfGrl7L3EYX02udrO73sbItwxFYwBdd5dikxegYJ5rZwhWYRWcaYGO7i7nFJl5sSXix083OsZJD0yCu8DwmzzCe5Azm0CDGoAp3ehh3fpjA2QTmuJJddweHPilHfhn7Hgm6QDcGvwxrSI4j2oc73ocr1oM7JiOQlhMtKEmVNKTLA2TOhsiej5K/HKd0PcPZ7QLlm3nKN/OULhd4+/aUo/1eXIY5fvq2xM9en/GjmxxJ+zE702qWh6RsTys5XBom4zMTsZ5wk41zV0xzV0rycJamEPMS91rIhJzkIi7KST8vy3GKUSf3xSgh2xGlmIuff3bN9z99x1evz/ny9RU/en/Lr3/2GT/6cMWPv7jgn/7bl9zcWMiVNkjmZ/DEFFgCYnSOZnT2dg4sIrb0bZy65WwbGtgx1v2Q03p2TuvZNTSyZ2zi2CZCb+/ixCFFZ+ni0NjJgUHE4WkF7+5JE9u6BjaPGtg4rOfFfgPre/Ws7tSxsl3LwounLLx4wsKLJyxuCixvC9UG3jx+zq6xhkNrXXXHWWdv+ARtdXS21VVir+XUUYfR1YDJ3YjB3ciJs4FTez0GRwMmVwWt2SXC5OzA+njzyROWE0woieU0pEpDZC5HyN+MV08Url/PcvNmjru389y9nePl2wVev1vg1ZsFXr6Z4+5hhuvbSS6uxiidDVVH6FCkG3+wC5e7E6dLhMslwuHswGZvw2xpxmRu4sRYz5GhnkNDPcfW5spRUKgLV6znsXkrD0T44z14ozKcQTE2XwdmdyuHxuccGJ6xf/qU/dOnHBiecWSqQWep48TWgNDeJtAtq2V1bQSprB6FqhORuI62zhp6+tsRngkIzwUaWyrHSG2dAmKZgEbbxOikFLG8iZpWgYYugQapgHL6GcPLDYystjCy2k6HQuBps0CjSECoEahtEBgYbkM7VI9a+wy5RmDndJBQdhejZxaDYwpncBlXYJn1nV4S+T0+/0mMZG6PA72agyMly2vtbGx3YfdOYXJOcGAcwOafJpJdJ1xYIXG+hj87jSs1gjM5TPBsGkdmkB2niBfWJl7Ymtn1iDDEKjvP9kgvrmg/7njlTqov2U0spyB9piF3PkiqqCJT0lK6Gq8+CVS8muXq5SpXN0u8er3L3c0+TtsYSxMiPtzG+fHrIv/nn77ldTHE8coou3Ma1sf7mFZLcBxtcrQyz1U6ym0uRsRlJOm3kfBZ8VmOCTsNBGx6bLptLpIBXpVT/N03X/Cnb7+mGHFTinr4+198xe9+/iW/+elXfP3hFT/78h2//9XX/ORH9/z533/B7b0Dl3ccT0SLK/KXV0mHcEU07Bsk7BlE7Bgb2THVsGuuZcdUw46hlh1DLbvGOvZM9eybmji0tHJk7eDA3MbeaSt7p5VxeVv3EW8dLw7qWNurYW23jtWdOpa36ljarGVuTWB+/Qnz68Ij4KfVXeht/XP2TLUc2eqrx0U/AH5ebV2dva4K1OCsx+hqwOxpwuJtxuxrxeRtwexuxuxuxuqt7NDavRLsXgkOXxfuYDf+WD+RjIpEYbDyRnwzTul+ivNX01y/nuX27TwP7xd5eL/Iq3eLvHm3xNv3y7x5t8SrNwvcv5zl9n6ay+txSmdDZPNqUhkFoUg3voAEj0eMyyXC7e6sIHa0Y7O1YrY0YzA3cmpq4tjUyIm1BaNbhD3cjTsuf1z7ygkk5Phi3XgiUhyBTqze9v9/gOtqBZ4/ExBLntEjb6KntxWRuJa2zhqe1QnUNws8q6vsIEtkNfT0NTI43MnoeBf9qmbqWgWEBoHadoEmqYB6ppahpXqGlhpZ2OtDMdqM0Cgg1Ak8qa/8O3JFPf39zxkabWJhTYon8oJj8ziasVrm1rpwBlcxOaZZ3pChN49xfmcmf6bD5Z/HZB9ncU3EyNQz5ldbcQUX2Drs49CkIZRZxuofwZ+ZxR4dxhBQYIloCJ3PEDyfxBDtZ88tZscpYs8tRhfoxhjqwRrtxZ1QVp5aSikIZftIFFSkymoyZQ3pkppseYji5RiFyyly5xPkL6Yr1xov57m8WufVgx6rcQRt7zMshwtkfEZ++6OX/Onn7zBuTpNwHGDbW+DFlJrVcQ2Owy2sB1sUwm58lmNOd1c5fDGPXb+Dz3KMz3JMIeYlH/Xw2f0ZL88z3JdT/Pann3GVi/L6Msd3P/ucH79/xX/84x/4u19/w08+f+C7X3/Bv//rL3j/IcLbD3bO7jaJF8bwJRSYPJ3oHZVG3TlpZ/e0gx1jI7umenZN9ZUG/i+Ad04riA/MLewZm9g9qWRH38y2romt4/rHXeYaVnefs7Jdw9LmcxY3aphff8bMisDsqsDc2scxutK+27o6dk9rObDUc2xv4NTVXF2//lfAekc9RmcjJlcTJncjFm8zNn8r9kAbrrAIZ6gDR6ASd1CCJ9T1yQP0/qicSEpJMl/BW7we5/x+mqtXc1W4rz5b5vXnK7z9bIV3H1Z4/9kqHz6s8v79Cm/f/oD4+naS8vkw6byKeLqPQEj6CFiE292BxyOuYnY6O7DaWrHYWysjta0Vg70Nk6cTe7i78rRcrALXF+vGG5XhDnfhCHRi8bRhcrV8Anjv5An7p085ND5Hb63n1N6I0CURaGoUaKgXkMpqaWkVaOt4Rp9CREOLQGOrwNPayigtkdWh1IiYnlMyOS1HLH1ObYtAY6dAi1SgtUdANV3D0FIjA3P1LOz1Mb7UTW17pcVrGgTaRE8QSZ7SJRMYm+xgcl6EM7jK8kYv8+tSlrfk2P3LTM63MzHXgt27QCixxdm1kfMbE97QEkfGQfZ0/SxtiDg4VWHzThHJrOGKTGPyaDl/dUDxbgtzUI3O1YM9PoAzqeUkJOfYL0Mfklc/tkT7cCZU+NIaAlkN0fwA8dIAqZKGVFlNuqSmeDVS+Q0X5yPkL8cp38xSvpknezbF9e0q5bMVvvjgxGGZYEzTzKy2i5ONOcpRF//XP/yKHz/kidv3uU172F0Y5nBlmpjdgP1oh5jTiPlwg3TQwV0pyUUmzH05xVUuSthpIOm3YdfvMNQnpqejHqWkjV//5AM/effAF6+uyUUDnKeT/PPff8d//5e/58//8h2/+dUrvv32jFxxh0R2DndEgyMk58TRwaGllROHlAOTmH2jiF1jC3vmJvbMTRXIj4g/At41NHBgbubQ0sq+qZm900p2T1qqgD+27w94nzG//ozZ1SdMLwvMrPwAeHW30r6VSxwNHNka0TkaK0c/jzvIHwEfWWvQ2es4cTZgcjVh8bRg9bVgD7ThDHXgCovwxbvwxiT4ol2VRGSVX1kTleOPygnG+37AW658H8/vp7l5Nc/dmwrcd5+v8uHLdT776gWffbHOZ1+s8/nn63zxxQs++2yNDx9WefNuiZev57m5m6J0NkQqpySalOMPduH1i/F6O/F6O/H5uvD5uvB6Jbg9YhzODhxuETaPuDrWm71iHJEevMnKrvNfAnaFJNj9oirgI1PN/xqw172DWCzQ1SXw5JlAa1vlgkZzq4Dw9IfUNQg0tz5BJKqlX9FBb18LDc0CraInyJQNdKme0dItVO4+z9bRN/KE4fl2NOMdiHpqqGuqvBE0NguIJM/pFD+lp/c5C8tytg4GUGqeM78qY3a5i7XtPgZGazk2jmFyTHNsHMEfWyNZ2McZmMfomsBgH2P7qJfto1701gHC6VUimRXKtwecvdwne7VO8myReHmOxOU8kfI0tpiGY08XRz4pxlAvhqAcW7Qfe0yBM9aPI1oZoUNZBfG8knheSaY8QPZskPz5CMXLscqv1bmf5ex2jtJ1pX0vL7d5dW/CYZ5B3VODtq+VpVEVftMhX9zm+PMfvyEXNJD06Ei49DiOXuDU7ZD2OYjYT7lMBflwXeDL+zN+8eVrvnp9RdRtwqbbZnFCy8RAHz7rCT96f89PP3tDw1OBr9+95A+/+oYfv3tDKuDjw/0Vf/jNz/h//59/5e+//5xvf37G9Y2JcHQGh1eFJ6rGGerD5JZh9sjR2aQcmDrZM7Wyb2l53I1uYtfQUMkj4ANLI0e2Zo5tbRw8PhZYea63ka3/j7H3+mpzT9M2XwFKKKOERM45i4xyRBJCEgiRc8ZgsHGOBBuwCfbeVV3VVR2qu6pz91fzdc9aMwczczD/2DUHL9beru7+1hw8y3JYPtLF/aT7+Y0qcEeUOENyhnyy7+EdzKdzQKCjX6BzQFThPrdY/3pGZTkb4WhGRSyrys1vY1OFjE4qcrVvNCNnbLqQ9KyGyXkdU0t6pleKmF0zMb/xk093ZaP4PmysbNhY3ixlebOUtZ0qNh/U8uCwnqMnLTx50cGzN2KN+/58gLOLYT5eOrn85Obyk5urKzeXly4+Xbr4fP/548cRzs4Gefu+l+cvO3j4uIGtvQpWNsT0eW7BxNyCiflFM4uLFhYXLSwsFjO/YGVm1sTMvJnsgpnJedGNlJ43fwfw3Foxc2vFzK5amV425+rf8ayaSFJKeLwgB/C3FDoHcHZqiLqaQuRSAaNRoLJChbVYhqGoAKlcVF6VJg+5UkAqF9BqCzCZFOh0+UjlAmq9gKVchqlCgsYmYK4SaOrVUN2uoLHHgK1Kid4qRakRUGnzMRgVqDQCykIBo1mgqlZJSblAVa2UgeFShlxlNLaraXfoSU5153akp5f6WNgYYXXPw/KOi+RMO7HJBuKTdbiDZkZTlYxna1l70M/YVAWhcQvTaw2s7nexfdzHw1dONp84SC6VE54y3q9P2glOGghMaPEmlDijBfgSMsayWjILRUzO61jZLmFp08b6biUPn7Tw8Fk7O0eN7B618PhlPw8e9vH0SZCTN1k8w2WUmiSoJALlBiUDLTU83pjlN9fv+Hp+TCrg4PnuAovJIA9XZ7l4+Yir1485ffKA63dPefFgjcXJGEdbi5y/POL9033+8NtfMJMc5dXxAdlklNuLE+4uTzFp5CxlJ/mbX/2SizeveP34gA+vn/APf/0D/9f/8Xv+8Q9X/PmfPebF8yhLq23MrdSRnislNK4lkioikjYSThURTBkIpfUEUzqxI/3/A2DRVfQzgINKhnxy+t0F9DoL6BnOo2sgj44+4TsF7vdIGAkW4I3KcyuUoxkV8ftVyfFZba6RFc3IiWbkxKbE8dDkvI7s0k/gLm6J3tz1HRsbu3Y2dkpZ3y659+mWsLpdzvq93W/nYQOHx808edHB89fdvH7fx/vzAc4/DnNx5eLqys3nz16ur318/uzl5mdx/cnD5aWL8/Mh3rxz8OxFOweP6tncLWdlw5YDd2HJwsKShaUlK0tLVhaXbCwu2ZhbtDK3aGVmycrUovmnzbAVW65h+g3eb+nzxJye8ayasUxhDuBQIp9QIp/weAGjKRnxSeU9wBN97O3Gqa9T4nbXUV6mQKMV0Ook5OUJGIwKLFYdhap88gsE9HoFRqMSrbaAIpMce7kGW5USS0UelkoBS6VAs8NATZuK+o4iTLZ8NAbxB0CRUYXFqiO/QKCgQKDYpsRoFLDZBEZGKnF5q2loKaS2UU4o3kh62kFbj4YRv52xTCu+WCW+WCWZ5V4mFnsYm2rCE7EyMdvA3GoHrmARgx41Y1MVJLLluEc1DAUU+OJ6ZtYaWHvYxeRyNcGUgUCyiEBST49PgsMv4PAKdLsFBgMCgYSc0ZSC4FgeqRktCxt2th7WsnlQk2v3rz6oY+ewjcPHQ7x5l+TsZI62Ji3lVikKQcBaWEClUUNosJOnu/P84vI5n9895PTpNm8fbfJke5mt2TRziSALqSCTYRdDbbV0N1YQGulhLh3j6f4mb58c8off/hkf377i+vyEw70tfvPLrxQbdRQVygk7h7l6+5qb07c83dvg939+x3/86294/3KVX//ymPdvsywstTM1V0NmriK38jg6YSKcKiKULiKU1osQpzUExgvxJ5RiAyulJJiSE5ksZDSjIZQuzJ3G8ceVeKNKceYb0TASUDLoVdDvUtA7IqNnqICugbxcDewYkXwH8M8V+E8Bjk0VEpsSlzPGpgtJzmnILhUxt2ZhYdPK8o6d1b1SNh6Us71fxu5+Obv7lew8qGBrT4ztB9Xs7Nfw4LCeh4+bePysjeevu3nzzsH70/7v4L357OXuxs+X24AY136+3oifbz57+XTp4uxskDfvHDx93sb+UR3re98DvLhsZWml+LtYXrWxsGwTFzzuAf5mDcysWJlaFbfjsqsWsqsWMssm0gsGEjOaXDMvlJJ+F+G0jNFJRW7FVMhO9rK27KW2Mp8ivUBdjZahwTpaWktRa8S02mzRotXJkeQJSKUCeXkCSqWEugY7g85WWnpKqGrWUdlUSFGpQGl9AUXFAgargEwlUGRRUKjKRyoTUCjFX1WqfMxmJSajmL63thbR02OluETA7a9mLNVOVYOEkmqBuvZ8EtlWXOFSajsKiGdbmd8aYSRip2tQgT9qZWmzh+WtHnyjZjLz9cystjC1VE84acUXKyI9V83SThupuQqcYRU9TgmtfQJdTgGHP4/hsBRXTEYopWFs2kgsoyUQzyc8LmP3UQNP3/aytFVOetbA8nYFGw8bmVuv4sHRIOcfZlhfcVFXJUMtFaiwaLBpC2ksMePubmJ9Os580kc61Md03EVm1MnBygwz8QAxVy/d9XZaKyw0lRppq7Hj7W8nMOzA09/J0lSKrcUZvl595ObDKadvXrCzvoxrsI9kJMhIVwfJoJfLNy84WFtgbWacw61pTl6s87vfvuX3f/2aR4dhFpbayczWEh43EU4aCKf1hFK6e4CLfgaw6r8FODyhIpS8j3GxFg4livDHivBEdDgDWoZ9agbcSvqcchzDUroHxVr4O4Bj31Jo5X8C+Fsj6xu833afZ1ZMLGwU/wTvQQVbB5XsHVayf1jF/mENDx5Ws7Nfxc5+FbsHYtp88Kgxlzq/fOvg3Ukfp+eDfLx05uC9vfbx5TbA17sgP9yK8eNdiB/vQtxe+3IAv37bw5NnrTmAlzZtzC6aROW9B/ZPY2m1hLkVG9OLFqbuL21MLltykV21MLViZmrFzOSSkeScjnhWXCENJgu+A1dcKVXcZyj3AK8tulhZdLM876a8TIK9WIJOK2AtVhAKD1JZaUGjyUcqFaErKBCQSAQsFi0Go4zewUaGPc2U16gxl0goqZKiM4uptVIt7korVQJSmUCBVECuyEcmz0NZWIDRqKTIIGA0CBgMAl1dZrq6jYSiTTgGzFQ35tHQLsVgFxgO2JjdHKDfbyaYqmFiuZvIZC19LhU9I3I8oyZikxVML7UwNlnJ+FQ1yWwVa7sOVra7cYf0zKw0Ek3baOwUa7PhoJLhUCFjc2WEJi3Ep4uZXqsiNiX6WCcXbbhDAnNrdibnzCxuljG/WUYoKSezZGHzYSvHL/ycfZzD4ynHqBOwaCUUKQVK9GoqzQZ66svpb6lgoLUcj6OWuN9BMjRCaKQHd28rIx31VJoU1Nu1VJoUtFYX09VQRk9TJb0t1bj7OphNRZlJx3l1fMCn03f8+PmSf/u7v+XXX254+/iI4801tmYnWZ6IMdJRT3lRAaUGgdn0ML/9xXM+fVxjabmP9FQtiUwFIwEF/oSaUEqHL6ElkBQVODypI5TSiF3ndCHhCRGw6JSayKSaYEp0D0Un9PduIxPxSRvRVAmRcRvBeDG+UTOuoIFBj5o+p5zuQbEm7nXm5ZpY3zawImklsayG2P0I6Zv6il9OUYGTcxomFvXMrVlY2rKzeqakRAAAIABJREFU9qBMhPewip3Dag6Oqjl4+BPADw5rxXjUyINHjRw96+Dxi65c6nx6PsyHCxeXl6L63t34c+r79S7IL76E+cWXMD/ehfjhNsj1Jw9XF05OTwdyTayjJ01sHVSJLq9lE/PLJhaXzSyvWlldL2Fto5TV9RIR4DUb86tiN3z63i44sfRTTK2Yc8r78x9g0YyS0UnFf6m63/4+mlEilJgFnj+e4O7TPsvzXuqrVBTkCWg1AhqtgNPZRX19KTqdFIlEQKkUUKkkSCRiU6uxxc6wp5mm9mLsFUrsZQoMJnFcJFWITSutXopKXYBckYdUJiEvXwTZaFJjt8mprlRSU6NiZKQSj7eaULQBx4CR9l419W351LYJ9LmLiE83klroYPXAxfRGP1Or3UQnK+lzF9LnLiQwVszCemfOzzq/1kkgVszKtgN3qIjWHgFP2Eh6pgbfqJHRtA3fmJHWIYG2YYHWIYFYxsLMejXeqJJE1sRoUs3ErAVXUIIrKOAezWPQL5CY1jGzWsHadjfPXo7jdJZg0AmYtAI6mYBFJadEr6bGoqWztpiB1nLcPTX4B9twOZrprC+jqcJCmV5OhVGOSSFQY1XRUmWludJCxNWLp68NR0stTkcbR9trHO6scfL8MR/ePufT+7fcnL/j6fYGV6+f8nJ/g9v3z1jLRKk2ybEWCpQaBDbngsTDzcQidaTSjTi9WubWGolMGPDGCwmmDPd1cBGhCS2h1D2oaUUuIpNiHRxKqYmktMQzJtH3m7GRnK5gfKqa+EQFo8lSQmO2HMTDPi09QwU5gPs9kp8BrCCSVhKdKiSWVX0H758CPLlkYG7NwvJ2Cev75Ww+rGT7qJq9R7U8fFTDw8NqEeDDGvaP6njwuP5+YtCcu+P94l0/784GOf/o5OLKw+fPfq6vfdzeBr4D+IcvoRzAX24DOQU+Px/i3Ukfr9508/hZC7tHtaIKr1lZWrOyvGplZa2YtY3SXCyv369YroiWwamfAZxaMpFaMjG5ZBQdVPP63BbZN3h/Wmj5z+qbA9ikEqgtF3i0n2RtKcCzx7McHsxgt8nRaUTldDgaaWurQi4X1VetFtDrxSaXwZRPR081Le1l2EpVmC1iA0yrkyCTCxQWCuj1MvR6BYWFeUhlApI8AUWhlGKbAZNRgs0mobS0gPb2Ioac5Qy7S2nrLqSpU0Z9m4SOASWeUTueaDFdTg2ZlQ5W9p3MbPQwt9GFe9TEUEBHcNzOzEo76dkGFtZ62DvyMjpeQbujgIpqgdZOgfRULempWprbBeITFQyGtFS0CzQPCHR7pCSmS1nZaycxZWdqqZLklIVoSkvXgED3oMCgX4R4YsFCeq6YmcVGjo7D+P1VFFsELPo8dAoBY6EUq1ZOiUFBQ5mB9joLnQ0WuhtL6awvpaXaRmuNnSqrhnKTgroSHW01VjrqSvD0tRIa6Sbm7Wdi1IOjuQpXTxtDXc0Ehxzsrc6xNj3JswebXLw85uOzQ17tr/Orj695d7DOy50lnmxMY5IJNJVp+Pd//iWzmQH8/jKSE7UM+wvxxTV4x1SMZsyEJ4yE0kUE0xpCKTWhdCGhtIJgSkogWUAorSA8oSIyoSU2aSA5U0x6rpT0bAWTc3VMzDaQzNaSyFQTS1cQTpTgG7XgDhXROyKj11lArzPvfolDTKFFZ04ho5l7iH+mOuIXVf4dwPPrVlZ2xNR5+6iaveM69o/rOXxcz9Gjeh4e1XPwSKx3D46befisnYPnHTx53cfz9/28ORvm5MLNxSc/n28C3NwEub0NcXsb4ObGz82NX4T5HuKvd8FcTfz5s5eLCyen52In+tmrDg6PG9k9qBYbZpv2XPd7bcPO2oad1c1SVjZKmF+2MLtsERtUK+J9q/SiieSikfGFItILBpJzuu/U9xu8P4//CuTYVCHCUFcROplAf6eJ5VkPQ45SbCaBH+5eEwr2oNeKEGeno+gN+ZjMcvLyRbeSRivWyBa7CqtNTaFaVGi1Og+VSoJcLiCTib9Xq/NQKkWopTIBjV5JWYUJs6UAvV7AZBKoqpLS3WvBE6jCHSqjo6+QIZ8RZ9DEWLaOxHQtDo+W0UwNCzv97D33s3U0wuhEOZ6omXCqjPRsE/6YnVS2iY09Jw+fhBnxmejuVdDlkDMwrMUfsjIwrCaRqSaYLsU1ZmEgqMMdNxFMWhjLljC72kBqpozEpIURXx6N7QKd/QJDfoFAQsnUcgkT8yUsrDZz/CxKItFMRVkBFr2ATiFgUEgwq6XYdDKqrCqqipVUFctpKDPQXGmhpdpGZ0M5VVYNxdo8mitNDLRXM9BeSyIwhLe/jURgiNBIDwMtNXgdrTSXmXF1tTDYWsubw11+/fmMH85ec/bkAU835jh/vMPls31Oj7Z4vrXAWjqCv7eZf/+n3/CPf3vNaKSJWKKGZLaGkZCczEoF8eliRjNmgikd/qTqPn3+CV7/eP59HSyuYY5PW0nPlZJZqCKzUEtmvpHJuUbSM/Uks7WMTdQwOl5BIGrHGzEz4FbR55LmAHaGpDmAo5Oq7wD+UwUemy4kNa8ls1zEwkYxq7tlbB1Wsfu4lv2nDRw+a+LRk0aOHjdyeNzI4XEzD5+0iK9o3Ns9n74f5MXZCO8+evjw2c+nmxDXdxFu70a5/RLm5i7A9a2fzzc+rm9/gvjLvTLf3QW5ufFzdeXmw8UIJ2cDvHzbzfHzVh4+FufBGztlrG2VsLJhY3VdjJWNEpbX7d8dcZ9aEevf9KKJ8YUiEvM/wZuY0fyn9PkbrOG0jFBKmquJw2nZTwDvrQbxDZbSWCWlv8vKg80EZr1ASbGU9rYS3r7ZY7C/kbx8Ab1BQK4QMFsKKLbmU1auRqkSkCkEBIkYEkFAni8gyxdQyiTI8gUK70GWyQQUhQLKwjx0RSLANrsCtVZ0QpVV5lPTIMMXqmJqoQd3yE5bbwGNnQLVrQIjIQPxqWqcYSNt/fksbPWw/zJANFvFYMjAYMhAIFFKn1uPN1LK+FQzM0s9TMy0kUjV0zugoaU9nxF3EemJBrILHfjHywiky/AnS/An7XhjFpIz1azu9DAxV01sopg+Vx5N3eIc0xdTEJ8yML9Vzep+C8vbHTx+MUpqopWK8jwMKhFgvVzApCrAqBSw6fIpKcqn3FxAfbmB+nIjZSYNNr0Sm0FGTYmWCrMSl6OR7sZSGsoMOBrKcTSUU1esw9/bSszp4HB1hqXkKBPBEQ6Wpnl9sMHF0wc8Xpvi1d4iJ0fr7M2MsTIe5MFMmqVEmKuXj/l//uc/8se//yVn79bZ2vKSzNaSnC3DHVMSny4mMmkS3UjJwvvmlVwEOJVHMCW9T6PVxLNFpGZtTMyXMbVYQ3axkexiC1PzrUzONpOebiQ51UgsVUUoXoo/amXIq8kBPOj7yQccSipF435WlauBv9XBPzfvp+a1TK0YWdy0sbZXnlPfg2eNHD1v5tHTZh4/aeXoSYsYzzp4/NLB8dt+nrwf5vmZi9cfvZxeBbm4CXN9F+X2azQH8Oc7P59ufVzdePl0K0J8cxfIKfPdXZDb2wCfrr1cXLk4+zCUU+HHz1rYe1iTg/ibEi+vF+dS6z8FeGLJSHqxiPEFPYl5Hck53XcQ/7yU+HkaHUwW4E/k4U/k5Uwe0YwSYS7dy+OdBLV2CR2NWj6dHzDcV41MIqBWCsikAn5fJy9f7VBTp7839UuorCiko6uUqiozCqUkB69UIqCUFiDLk6BRylBIRYDlUnF0pLg3SKi0+ZhtKkzWAgxGgbIKOZU1MoxWgZ7+IsanWgklKuh1qukYyKehS2DQr2V0opz4VBXDQQPjM3Us7DqITVfjTdjocWvoGlJR1y5hwG0mlm5kZrGXjV0v46lmBoeNDI2YqG0QqKsXaGiT0D2sxj1m5/G7MY5P4uw/8/P8dIytgyGWt7oY9CpwjEgY9EqJTZqZWa1iYbOGtYMm9p/3sbrbxeHTEKmJVsrKBbQqAZM2D5OqALNaikWdT4lBit2QR5lJSl2ZnrqyIuyGQkyqAqrtOpqqTPQ0lTHcVUetXYtZJVBpLKSt0oq3p5nwYCfL42EygSFCvW301dpZHgtx8nCDxYSXtwdLbE6FmY4Msj83ztKYn2zAiaulgfcHD/i//7d/5o9/9+fcfTpmezvAwloPoxNW5rbq7lNoQ86VJAIsJZjOJ5jOJzwhv29maRibNokAz1UwtVjHzHIzM8ttzCx1ML3Yfg9yK+OZBqLJSkJjdkb8uvu1SkkOYG+sIAdwfFrN2P0W1jcVSsxovruy8Q3g9Qdi+vzgSb143vdFC4+ftXH8tI1HT1s5fNrOoxc9PH7Tx9OTEZ6fuXj5wcObSz9nNxGuvsS5/iHO7dc4t3cxPt+Gubjx8/HGk4ufQ/wN4Lu7INe3fq4+e/hwMcL7035evu3OpdL7R3W51cpvSry0ZmVxVUyfp5dFgDPLJiaWjKQWDDmAUwsGUvN6UvN6knO6/wTxz+H1jUnwjUkIjOfnIBbq7QLNlVION8eoLcsnHe/jL351hs0kQasSUMhFiI1mgRFXI9OzPuqbtJSV5dPUYqa+vhSFsoA8iUC+JA9lgQKtUkNhgQyDSoVKlodaKaCUC8jkIsAyhYC8UEBnlGKxyaiqU1PToKW5w4C9UqClW8mA20gkWYErZMQVLsIbNeMK6xn0qZhZbWFmtY3EdA2hiTLG5uuYXGllOGym0VFAaaPAoM9KarqDWLKRzGw3Xn85za1SRlxWBoaKGHGa6R5QU1on0DwgZ2Xfye7TAOsHIyzt9N43YnS0OgTcYTXJ6VJmVmvYOGhj46CFuc0qlveaWNpqZe+Rm9REC+UVAnqNgN2kwKKRYSzMx25QUG5RYjfmY9ULlBhllJlU2I0qSs0aqop1lJkLGeyqp7HSTKWlkLYaK71NZYy5HcSHu3C31zLcVMFUYICx4S52MlGy/iHWk0FWkwFmo8MsJzwsjjmZ9Pbgaa1ibKCH2ICDxysr/Pvvf8f/+3/+kb/41RmxWJOY3sZ0+Md1BFO6n5Y4kgr842LqHEznE56UEplUEJ5QEplU3wNcwsRcBdmleuZWxcuVcys9zK30MLPURXahg4mZFjGVTpbhDOjpc8rpGRb3oL+ZGILjiu8Azl3SuI/knIbUvJaJRT3ZVRNLW/afDCb3AB89b+b4ebsI8JP279T32amTFx89vL4K8P5zmA93UT7/kODmh3Fuv8a5uY3y6SbEhxsf59duzq/dfLh2c3nn4/PdPcB3Ae6+hrj7GuL2iwjxxZUrVwu/ftvDkxdtPHrazMGjenYPqtnaq2B9uzQHsdjAEiGevFff1IKB5KIY336fWjCQnNeTmNUSn1YTnRL7A9/KGO+YBHdMwBMX8I5J7ksbKUJzjRKTRmAx62TU28zGQoTrj4ckR/sw6wXMelGJ1SrRL2wvLaCx1UBjq4HiEhkWqw5BENVXIgjIJFI0ykIUBfnoVApUsjw0hRJUSjH9lisFChRih1pjyMNql9PYaqS8SkZbp4GmZgVtHQpq6gW8YRvDXgP+WDHOkAFP1Exqvpal3W4SM1XMbbbjGbOSXGhkbrsHX6IUh0tNi0OKO1JCPNNILN2Iy28nnmpiNFGP02enpVPB4LAJx5CWfq+O9mEZLQP5+MdsZBaamVoSj5pn5xqJjtvY3Ovl4fEIa3udbB91s33UyfRaBTPr1cytNbK1N0AiVU95mYBBJ1BilmNU56EpEDCp8ig1KSkxyjCpBQxKAZ1cTLPN2gLKzWrqy43UlOix6aRUF6vpqDEzGRok7u4gOtSCv6eWkeZSZiNDzIQG2JwIMh8aJBvoYzXpI+Xt5mgpRcrdQ3SglWxwmNmwl+PVRbYyE/zhV1/5j3/6S/71H37k5YtZxsYbmFlpxxnS4EvoCST1uRTaPy7HPy4lmBJtfqMZsSMdTmuIZ405gKeXG1hYb2dhvYv5VQfzqw7mVnqYXuy8V+E6YukKXMEicZw0JOQU2BMvIDguIzr1nxX4fwXwxn4lO0e1PHhSz8HTJo6et3D8vJ3HTzs4etbB4bMujl45OH43xPMzNy8vfLz9FOH0JsbFlzGuf0xy+2OSux/GcwCfX3s5uXZx9tnF+bWbi1svV1/8XH8RAf7yQ5gvP4S5+xri5i7A1WcP5xdDOZfSi1edPH3exqPjJg4e1bO3L0L8TYnn77vQ3wCe+Bmw6cWiXPx3APvHpfgSBXjiebhjEtwxCZ54Hr6EaMUUUuN9LMz5aKpTU2oR6GoycbA2zt35I5qqtJQYBQyFAlazQJFBdBJ1DpZTfb/jXKiRkZ8vbm1Jpfk5kNUqGVaLAaNeiVIp1r/iSCkfta6AQk0eKp2AvayQphYjXV1m+h1meru0dLTIqK8RqK8VGBjW4gkV0zmkoLk/j1CmnInVBsYWKphYqSWULGFioYnsShvJ2Tp8MQstvQKdQzIS07UEEqUMesV3mHpH9DR3yXEMF+GPVdPWp6TPo2Jus5WJ+RqGfEo8QQ2eoIaFhSbGxkro65WytNjO8moXC6utLG21MrVUSXa1kum1KqYXalhZ62J+oYfONjVmg0CpRYZFJ8WkkWLTKynWybDqCr4HWC6gVwiYVXkUa6WYVRJaKow4Gu3MxIeJOpsZHarD01FCa4mcQHc5y4khIj3VxPrqyLg6mQ304e+oJRscxtdZT6inlYV4kJXxGM931vn15zP2FjO8Od7gj//4S/7wuwseP5pkccnNeLKdmcVeEtlqkrPlRCYteMc0+BLqe3ODktCEikhGQ/h+lTKeNTI+bSM1U8bMUg1L660srXWyuNLNwnIXc4sdTM23k8o2EUtVEU6U4AoZGfAW4nDl0eeRMBzKwxXLxz8uJZSWEZ0q/GmUdL/QkZrXM7Eo3oKeWjQyu2plZaeczYMadg/refC4Xuw0P2kRH3F71s7Bkw4Onvdw9LKfpyduXn8M8/5TjLObJB/u0lx+TXP9Q5qbH5LcfBnj83WEq+sg5588OfX9cO3m/LOLD9duPt16ufka4O6HILdfA9x+DXDzxc/1jZerTyOcfxjk5LSPt28dvHrVw7NnHWIm8KiZ/Yf1bO1VsbpZysJaCfOr9vtroJbvnk3JLInbV1Mr4obW+Iw+d0p3LCveBwsk5PjiUtyjeTjDAiMhCc6weAzBG5MhtLQbWVwJ4XDYUckFLFqBxjIll692OVhJos0Xv3QWgyBuTdnzaeiwUtddjMYiobKmFI1GhSDcL2rIxBmxXC5QXKzFaFSi00lFiBUCBXLREyxTi2d2TLZ86hv19HRbGB6w4RqwMNiloblOoKZSwNGrwhsuwRmyMRAoIpAuIzRdjnfCTDhrY9CvJT5ZRSJbTSJbjS9mot+jpGukgGCyGH/CwkjIgCtiwhuz4XDqGQ7YiEw00ecz0e2UsrLfweJ2C0tbLeweOnD7lHR1iT+0TAYBn9dIMFzMwEghjsF8epwS/GOiq2d6rprV1U6WFrtxdGmxFAkUGwUs+gLsRhUWjQyrVopVV4BVl4dFJ2DRCZi1AmaNCLBCEGgsKaKhREc2OoTXUUXc2Uiwv4yEq47RwUrmRnsY7a/C317CcmyABr3A2EADw412In3NpD19LI9HeLa9xsu9Td4e7fHbrxfcnD/h7uox//IP11xf7rG5EWZ7K8LaepDMjIPsQhvRCTuBcTPhCTPBlAFXVIEzLseXVhGaECMyoSU2VUQiayE1W8Lsci3LGy0srrWxsNLB/HI7M/NtTM42k5isJZIoJ3CfOfV7lPQ4JfR5JAyGJLhi+fiSUoITPwH8TYnF07GG3CH3mRULCxt2Vncr2HpYKwL8qFGc8z5u4uBpGwdP23jwtIP9Z90cvR7g2amHNxcRTj4ncgB/+vJfA3x25ebsHtrzz67vAL7+4ufuh2AOYhFgN1efRvjwcYDTs35O3vXy7nUvL150iRAft/LwqJGd/RrWt8tZXBchnl21Mb1s/QncJQtTy9YcwD+/By3ehNYRndTgi0vxRPNxhvMYCohTkJFgnpjJjMoQXL4mWtrMuN0NTCQG0CkEYv5ONudG+cXVM9qrxTGTXilgMQnYSqTUNhVhq1GgMAiodTLyCwTksjxqqsvo6WzCbtWSL4iNK51GglotzoOlcvGyR4Fc3NBS6yUYi/OoqiukpUVHW7OKvk4tjvZCWhsk1FQI1NYJOAb1eEbL8cRLGYlaGRotYiSmx5MwMuDTEJ2sJJIuIz5VRTxbQWKmAt+YkVDKii9pxh03EUiVEJ6soM9XRI/bgDNSRvtQId5EMQ9fuhjLljCeLeXNaYSDowEWFproaBcwmwRaWwR6HAqcHgNOr5bmHoGOQQFPVEMsYWZ+vpnNjUFGQzU01qqwmSQYVAImTT7FOpkYeinF+nxsRXnYivIoNkiw6vKotKgpNygo0eXT11xKa4WW/hYz9VaB3gYNXVUKVtMD9NaoCHXbifdXM+1vJ+aoJtxdwVrSTTbUx0rKx9ZUnOONRU4fP+Dq9TH/9vtf8dd/9oG//90lf/WbN2xvBHjwIMLWlpf0ZAexZAMTsw3MrDYxv9HKWNbOSERBIKklNm3EMyYnNCGq8Tdjf2zSyNiUmcxcGXMrdcyvNDG72ML0fBNTs40ks7VEUyK8nrCJ4YCOPreMHqeEXrfAYEiCM5p3D7BChPe+C/1zeKeXzcysWJhft7G0Vcr6gyq2D+vYPaxn76iBvaMGcdvquIUHxy3sPWn/DuC3l6OcXo9xfjvOh7skn79O5AC+vovz+TrC5bWPs09OTj+P5OLs0wgfrl1c3Xj4fOfj7hcBbn/0c/PFy/Wdh8+3bi4/iwCfnQ9wdiJC/Pp1Ny9edOTS6QeHogqvbJWxtPEzs/792uTUsjUH8Ld1yonFIpKzBhLTOmIZNZG0Mqe8QwGBAZ8YQwHhJ4A3d1MYzAKVlQom08N0txTz/vkGFdYCxgPdjHm7sGgE7EaBYouEEruUlg4Ltgop1lIZ8vtOtUIhUFNTgnOoi4Y6G+pCAZ1GQF0oNrBUSgGdLh+NrgCZUqyBlRrxVlZ5tZzGRjXVVXm0N8tpbyygtT6P2kqB8nKBhtYChv12vPEq+v1GHF41A0Ed/fevFXpjFjxRM+5oEVMrDSxst5NeqCGQNhNIW3GPFeFNWAhNlNHvN9A6oKDLqaOuS3yaZfN4kNR8JdmVOh6/8nD01MXjp14iURsupwGny4TTVYQvaGHEr6ehU6ChW8AZVuH2qZiaqmVrY4SZqR76um3YzXloFWLp8S19tupEcEvNUsqtciqKFVRaFVQY5dTbtTSWaqmxyik3CHTUaOioVOKoVxMfqcbTYWbS00i0r5xAh53jpSjjA/VMuJpYGR9ketTB9kyIo/VJXh+scfP+GX/19ZL/+Off8j/+4Rf8+Y8v+OUPR2xsuFla7iMz1cxkthn/qI3ZlTacIR3uiI70fCXJ+XISs+KZoeSClWhWR3hSTTitIZzWMJrWEZ0oIjllITNXJpokZmuZmK4mlRE3ssIJcQbsChoY9KnodUm/U2BntABvQkZ44qed3sS9+k4sFjG1YmZ21crcWjFLW6Ws7laweVDD9mHd9/D+J4A7efS6n2enLt5chji5jnJ+O87Fl3E+f01x/WOS668JPn2JcnUd5OKzl5PLYd5fDeXi9GqY889OLu9V+PZHPzc/+HLwfrpxcfl5hI8Xg5x/6Of8tJ/T9328edMjQnx/tUNsatWytlPB6nZ57uLGz80L3wwM3z5PLonnb8dn9MQyakJJOa6IhJGQuEDU7xVjOCjgjkjxxRQIA65aVjbiZLJedFqB/e0MvuEmSookjPRU88urF0Tc7RQbxIaWxSRgs0nEsU+tBkNRASaDAo1KwGJS0NpYSkONlWKTgjK7CqMuPwewxarEYilEUSi+7qDUCKgNAqXVMjq6zbS2qBl0mOhokdHSkEd1pUBjYwENLTKGPHZCiTpc4VIGvEU4XGoaugTqOwWaHQKdTjltQ3lMb7Ywt9NKZq0eT0LPaNaOK6FnaFRLcLIEz3gxAyEDw6MWutyFVHUKhCZthDPF7D8f4cVZmJ2jAXYfDuILmfD6LGRnO1jdGCI51YgrYMLhVNA+KNA5JDDsUpIcr2BhrpvJVCe9nXaKi/IwaiTYjUrM6nxMaglmjYBVL1BqllJdoqS+QkdzVRFVZjltFUWEBptoKlXR32imp1aHs81Cd7WSTKAZb6eFULeNSU8DiaFqJj2NrI0NiJ8DzSwme9meCfB8b5azJ9v8ePGKP/7+V/yPv/sF//T7G/7nv97x45d90hNNLK/2EBsvJRQzk11sxB+1EExYiWdKCSSKGAjIGM0YmVwpxTeuZGy2iGhWx2hGw+ikjlhGL25jZU1MzNhIZkrEI3eTJT/bibbgCRtxBrQMegpFi+E3gAN5OCP5+BL31zgmFTmAU/N6sksmZletzK/bWNiws7JTztpeJVsPa9k5qv8O3v3HTTw4FmP/SSsHz7t49Lqf5ycu3nz08/4qxIfbMS6+jPHpa4LPPyT4/DXG5W2Ii09+Ply5eX8xxNuLAd587OftxQDvrwY4/TzEh+sRPt44uf7q5dOdm6vbES5vhrn8PMTFp0E+XPZzfiGq8MlpH+/eO3jztptXb7t49rKNw+MGHhzWsrlfyfpeOcvbJSxsFDO3ZmF21Ux21ZQDeHpNPLqfU+E5XQ5gMX3+CeABn1gHfzvJK7x8v8nOwwzpKTcHh7M8O16hrlKDXilQX6ZmfTrCu6cbJEI9NNVoMOkESu0SAoFmampUaNQCZqOMIr0EoyGP6gotDbVGqso1VJVrsJryUSkEVAoBi0mOzapCq81Dqy3AUlyIzipQViOny2GltU3D8ICV1kYpbU0yGuvyaGqSU1WXR0evEV+0juBYLZ5wGY5hHdXNAuWNAnUdAp1OKQ6fnMUHXczvtjG92UhspoTMRj3+CRMDkULCUyWMzdcQnqpgdLoKX8qOw69gKKpmIKRk+aCTrThDAAAgAElEQVST4/c+Xn+M8vhlgEDETFunBG/YxsJaL0sb/UwttpDIVjMUVNE9nEffgIzRiJ10sonkWDt9XSXYjPlY9AUUG+SY1flYtPlYtBIsOhFiu1FCRbGCGruKzloLzs5KfL21hAbr6anVMdBURLNdQne1klBvCcPNejytRqJ9ZcwE20iO1LIc7WUjNchyysHOooeNGS9Hm2neH2/y49Ub/vd/+Qv+7e9/wT//4Zp//LuP/PLPDphf7CAyZsUf0ZKYLMYf0ZKariYwZiE6YSc5W44rqqbbJTASkRHJ6IlkdATThQRTSsITKqKTOqKTOsYmDYxnzMTTJmIpM5GkmfC4iVDC/JM7KahmwCOn15lHj1PIATwSzscTkxFMieuUsalCxme1TCwW5Wpe8dX7Mtb2KnPqu/voZ8r7XwB8+LyDR697eXYywuuPXt5dBfhwG+PjlxhXX+J8+hrj05eIOPv95OHkcpg3H/t5dd6bi7cXfby/GuDs0xBnn4a4vHFycT3Ch08DfPg0wMcrMUSA+zg77+PktI/3J70ixO97ePG6g0dPm9g/qmX7YTWb+5Ws7paxuGm7f+LmG8Bi/fu/AvhbA2s4KMI76BdwRfLxjykIp9QIjqFyQvFO1rfHmci4OTqYI+zrwG6UUFNayFBXJW+O17g+P+LDux0mE32UWAXqqmTUVSnQqQWKdOL8U6sSGzj1VVramyx0NFspNgoY9QJGvQRTUT6mIilFRikWSyH2MjVF9jzM5QKNbRrq6mUM9JqoqxZobiigqV5KTXU+xaUCFbXixQ5fpBZ/pJIht4WOXjmN3QJdLgUDQQ3hjJ2pjWayW01Mrtex9LCdhf1WQlNWHGE5wayN1FodsYUqYgs1jM5V0eWXEsiaic7aWNxvZfvYwaPXTuY2mhj2qWnuEqhtFhh06xhNVZKaqSc1W0MwaSGSKmbEpWYsXsn4WAOJWCsDPWXYTQWYdfnolQLFein2IjmlJjklpgKKDd8gzqfcLKO73kJPvTkHcKC3EnenDWeHmehIFQlXLUlPPVPBFlKeehZGO1kY7WQp1sOjlTCLqS42Zkc4WI3y/skKv7l9zz/99Y/869/+GX/72yv+8lfv+P3vTnj9JkMyXcX4ZCmxlJFQXE0wpmE8W85o0o4rrGU4qCSYMjK5XM74nA13XE4gpcKbUOBLiMBF0uJxttGkmmhKSySlF19hSIgH3gNxA76oDndYjTNYyIBHSp9LvEzZ75Ew6BOfVvlmaIhPqkhMqUjP6sgumXI17+puRU55v8G791gE9+dpdO7Pj8VjC8evenl5Msy7jx5Or/x8vBnl4jbC1ZcoV18jXN2F+fDZy+mli/cXQ7w67+XZWRdPTzt5dtbF6w8O3l328/5qgJNPg5x9HuDkqo/Ti15OL3o5u+zj/Ko/B/D5h35Oz3p5f+bg/ZmDtyfdvHrbwfGLJh4e17JzWM3WQSXre2Usb9mY37Awu2ZiZtXIzKqR6WUjs6tm5tYsTC8bySwUMTGnJzGlIpqW555VcQUFhn0CI34BbySPcEJBLK1G8IZb6HdWEU308PBohoH+auZnQmInVSdg0wtsLEQ5e7XJZLyX7ZUwv/nxGQ82RmmuLUSrFNCoBIru0+sSi0BdZSEt9XoaqtWUFRdQU66iskyN1SRFrxUo0udTZJSiN+ahMopvJ9U2KalvluN0llBfI1BXLaG8TKC2TobVLmApESEfdpfjDVbj9Zfi9FtpH5AxHCmi16cmNl2JZ9xEZNpOMGNh82kvc3vNBCaNdAUKCGStTGw0kFiuZnyljvGVGkIzVoIzJrxpNYsPmzg+c3H0ZphwqghXRE1Ns0BTp0AgbmM0VU50opzJhXri2RLimVKS6Spmsu0kYo2EfHV0tlixGiRYDVIMhQIWTQGlJiXVdg21ZRqqS1RU2RXUlmloqjTgaLIy1FbKuK+DTKSX2Vgf2UgXU5F2llJ9xEeqWUw4mIm0MxftZNxVSzbUyk7Gye6sh+e7YxxuRXj3ZJ7r8yP+/i9v+eM//IZfXL/j+sMTfv9Xl1x93GJh0UE8UcpYupip+VKWNmtY3qolNlHMWKaU0bSNQKII96iagUDenyiw+l4t1bmnQiLjYoSTGkLjWoIJTe5ipfgig3hq59td6F6XQL9HYNAnMByQ4B4tIJCQM5ZRM55VMzlvYGbFwuJmCau7Faw/qGJjv1pMmx835pT2G8C7h/XsHtazc1R//29EA8OT1328Oh3h/YWXs08BPt6EubgLcfklzOWXIJe3AT5cuzm5HObtxQDPzxw8Pe3kyUkHT087eXXew9uLPt5d9vPusp+Tqz7eXTh4/6GH9x96OPno4Oyyjw+XIsRiM6uPk/NeTs5FkF+/6+TJy2aOntaze1TD9sOq/xLg6ZWi/xbg8ayaaFpOJCknEM/HHRLhdQZ+Ajg+oUGYXvIws+zFYBHITDt59GiW/t5KTLr7JQ6pgKu/loinhV/dPWd9wU93o4aQs5InByk+nGySiPdgNooAN9QUUl0upcImoaZMSlOtlpZ6PY31Bop0AnabHINOQKUWUGkFdMUCWqtAaU0eNQ0FDA/bqKkRTf61NQVU1xRQWpFHSWUeZdUFdPaY8PlrGBmxM+Ky4ouV4k+WEEyXEp4sxTVWhC9tIpixsHzYwfaLXgIZAz2hAqILJYRn7SRXaxlbqhI/r5cRXy5mbMlMeEbH0sN6Hp0OM7FcwkCggD5PAR0DAt3DBYSTNvxxM/Mbraw+6GB+vYXsTDNTk+0kYq0M9ZVRXVaIzSjFopOiVwpopeIYrlifT02pmo7GYga6qhhxNODua2CwvZS4t41stJ+VSS/b8yEWUoPMJwc43k6wNeNmZ87LVtbFVtbFg3k/B4tBjpZGebqd4OXDCc7frPDx7Q6//eGEf/mbX/LbHy/51d05v//LG3794xueHWfITrcTH7cTTZpIZMQHt/xROamZcibmqpmYqyY6WYwnqmE4VMBwWIo7Lscbl+OJyfDGCvDFpYTG5OKh8jE5wbgCd0SKK1xwH7LcK4TfnlMZDkjFO9D+fJwBCc6ABE84n0BM/H/GMxomZ/XMrVjvb1hVsPmgmp2HdTlQ944axHT5XnF3jurZPqxj56ie3cNGdg8b2Ttq4uGTNp686uH16RDvP7pE99FNkIuboFj33vj5cOPj7LOLk6sR3l0O8uK8lyen3Tx538Wz0x5efBDj9QcHbz728ubcweuzHl6ddfL6vIuTj46cGp98dHD64fs4Oe/l7Uk3z9+08eRlM/vHteweVbG5X8HKtp3FDSvza2bm1sWYWTWKR/nuU+vpZSNTi+JNrLFMIeHxAgJxCd5RAU9EwDeaT2hMzmiyUFTg2mYFjqFisvMjDLuqsFgEdnZSDA3UoFcJlFnyMWoESk0CB1tjPNtPszozQm+zmuaqPFobVayu+Ply95jHjyaJR9qoLpdQaReor5TR1qSltVFHRZkMhUzAVlxAeZkSkzkfqUIE2FQmUF6TT1VdAUPDNtralJSVCZSWCdTUy+kdKsUxUIa1VKCiWoo/UEsk3IjHU0pzl5TmvgIaewX6A1p6fEpCGTvOMS1jC6VkNmpxjatwp/VE5mwEp4sJZK2Mr9SQWKlgfK2E9Jadqe1SYos6JtaKmX9QRSSjwzkqp99bQGufQItDwBnSMRJUk12pY+vIwe6jQXZ2XcxM9eB3V9PaYKC4KI8itUCRKh+NTMCikWHTy6mwqmmoKKK93kZPaym9bZX0tpeTGu0jFeom4mxizNPGasbH7mKE7fkQD5YjPNpM8HQryaO1OA+XR3m8Fufx+hgvdtKcPJrj/dMFrs/3+eHqOX/7mxv+9Q9/zt/8+o6/+vU1v/vNJy7P91hd9RCOlBIIFxGK64mmtCQyBsYmDYymzIymLITHTQTietyjhTgjMkZGC3BGpbi/xWgBnmg+gZiUYFxGKC7DH5XhG5V+B/G3z86Q+BLht9cIXcE8XEEJ7lDe/ZdQyWiykOSUlsycgfnVYla2yljfrWRrv4adh3U5tf353PdPAd47amLvqIkHj5o5fNrO09eOHMCnlx4+Xgf4eO3Lxc/hfX0xwLNzB8cnXRy/6+TpSTfPzxw8P+/m1XlPDuBXp928Ou3m9XkX7z/05OA9+ejg5Lznu3h/5uDNe/E1h+MXTd8BvLpTwtJmMQvrFuY3xPNA3258/SnA6Vld7iZWcCwPX1SE2B8tyKXPY5NahGSmjYERK919RqKJNmprFahUAunkILXVKgqlAnKJQF2ZjBKTwKuj/4+vt/xuNL/StZ8yMzODLFvMkiWLWRZZZrbLzMxYhsLuwGQyPcFJJunAdLo71Mlkzjn/2/V+eGx195l5z4e9rFVrVS1Xrbq84bf3fY8yNWBka9aDtjuP/lAPclkBclkhalUJd9fz/ORfz9jfHsCgqcCorcTj6MBsqqeqQqC0VHxbra3PoqRCoKEth7r2DJo7MpD05GK11aFUFtDaItDSKtAtL0RjrEFrrKexLYOaeoFOSRZqVQlOZwNmRyVqSwFqWwEmTykaew4Gdx5GbzaRsXpSL9twD5TiH66kL1qANVKA0Z9NZKKRyEQd/bM1pFbqGVmrp3+miMRcOcPLtcQmyomMlmPzZ6OyCKhMAnZ/Ie5wKWNzEtb2jOyfuDk6jjOc0qNXV9NSn0NFkUB5QQZVxblUFuVQW5JLU2UBnY1lyDuq0fY0YlK10avtwqbvwm2RkggamBx0MTfsZWEswPJkkLWZCOuzYfaWk5xsDHO0NsjBygCna0OcbY1yuzfF6/NFHq9W+cFH5/z8kzf84dc/4o+//Tlf/O6nfPn7n/DLn77lzetVFhbsROMt9CdriaYqiQwUEx0sITFSQTgl9q7e/hI8/YV4Y8V444W4o7k4wllPIS4SOMOZeCPZ+Ppz8EdEeL0REe7ncPdn4+7PxhPOwhXMSIc7lIknLMIbiGfRP1BAfKiQ0ckyJmereLncyMpGGytbHaztiAsb6WWNIzk7R/JvAby+L30aasnZOlCyfahi/1TH+ZWJ2/s+7l87eXzr5u1HHt5+5OHxg5uHDw4R3rc2Lt9YnrKvgYNbLfs3Go7uxCz8HOePJi4ejJzf67i403F5r+f60cjNaxM3r028em36Vun86sHIzYOBy1daTq+UHJ3L2DnoYmuvMw3w/Go9cyt1zK3UMb9az8xKLbOrdcys1KYz8th8OUNTJSRG84mkctJZ2BcVCMSziKSeZwfFCDPzJobGNUzN92KwVNEfVWA01tHU8IKqCoGqUoGGGoGSPAFJcyaLYw7uTyfolRezvxKkpyuL2hoBtaqEzo5MrOZa+nprCfm7WFsOEvRJCAV7MOirqKsTlS9rG0SDtJIKgbqWLMprBWrqBYymavw+CXJ5Hs1NAh2dmUhlRbRKc5Eqy5Bpy+nozqe2Tnwf9niacQWb6dHloHeWYfFVYg2WIzMLmP35DMy2M7LYhXewnN5wLjKrgMwqoHEJeAer8A6WE5+tZWC5jvh8Ob7RbJLzZczstDG62Ej/WAX+RCl6u4DSKGBx5uEOlTM83cnKjoH9Yy/7+3FiYRUySVl6hbK6JI/a0iIxSvJprCiks6EceUctOlkLVp0Uj1VN0Kkj7NKSCBqYGvSwMBliaaqfjYUkh2ujHK6NsvVS/Hy0PsbJ5hi3B3Pcny7y+myZd9frfPz6kE++d8uvfvI9vvjNT/jidz/lqy9/yd//9Et+/C9XbG9HGR5REOyvpT9ZTXyomthQGfHhMmJP0jjhVNlTD1uEL1GEN16Iqz+HvqA4MbaHMtMrfCKIWXhDWeLXbwDs7hdvfb2x3Kfs/ARt/wt8/Vn4YyK84YFsYoMFJEcKmZipZHahjqW1Fta+Ae/WQU96XfI5ngF+js1DOduHKnaO1Oweazg8N3B+ZeHmzpYG+M0HMR4+OMS+942Vq0eL2Ps+GDh8pWPvWs3ulYqDWy3Hr/Qc3WrTJfXZnZ7zewMXdwYxCz8YuH40puP2UQT39t7A7b2B63s9F7caTi4VHJ71pAFe3WljcaOJl2sNzK/W83KtgZdrDcyt1TO3Vs/sah1TS9VMLlYx/rLifwRYlJXNpn8w/2l2UIIwv2BgYEjK8pqDo5MksbiChUUfbreEsjKBhjqBuiqBmjIBeWc+VlUl8yMW1iYdjEYVTI1bkMnzaG4RaO8Q6Ourx+NpRqHIx2qtxWKpptdSR7c0l9a2F9TUigBLZSVIeoppbM2hvFq0bbHaGojHVPT21tAtzaWpTRxeNbRnIJGX0CEroKM7n05JFl1dmVisVXjDHch0BahMRbjCjThC1ThClVj9RfgHa4mMNeBJlmPwZKJ1itI57kQ5oZE6wuP1DC21MLnVQXSmHMfAC6LTJUxuNjM4V4stlEEgWYHO9oIejYDKKOANVjI21cPmno2jkxAri14Cnh6U3VV0tVbSVl9OfXkJNSWFVBXlU1daQEN5AS3VRSLE7XUYFR24LWpCLiMxv4WBiIX5iQg7qyPsro2zuzbK+d4cd2ernO/NcXO8xNXhAtfHC7y73uTjx32+c7/Hxw8HfPLdO37yw3f8+uf/xBe/+yl/+M2P+erLf+PPf/wRH3+0xcJiHyNjUmIpMftGB0sZGKtkYKyScLKQ+HA50cFSQgPFBBKFeGJ5OMJZ2AIvsHgF+oIZIsiBJ0uUYCbuQCbeUDbeSA7eSA6ucHZ6N9cby8YXz8Ef/dqUOhDPIBTPJpzIIpLMJjqYTXKkkNRYMTMva1lYaWJ1s53N3S629qRsP/W8O0dy0c7mRCm6YhzJ2TzsSQ+1to+V7B5r2DvWc3Rq5PTCxOVVL7ev7Nw/OHh87eLtezev37u4f2/n1VsbV48WLu5NnNwZOLrTs3+tY/tSzfa5ir0rLfvXOvZvRMuV41d6jl/pObkTHRwuHoxcPZjScf1o5ubBwM2Dges7I9d3Ri5v9Zxfazk+V3JwIksbgq/utLO02cLCehMv1xpZWG9iYb3xCeQ6ZpermV6sZGqhion5CoanSkiM5BEZyCSUeEEgKkYonkk0lUtiuIiB0RKEXlseQyMShkdkDI7I6bVVMjJqxB+Q0taWRUOdQHmxCHB7g4BFXY7HXIe5O4+Xo2aMujK6e3JoaRWoqBQzo91eh9/fxvi4Ea22CLO5CpW6CIOxSoSyXkBjrMLu6cTu7kKtq6G6WkClLMbrasPv78BmbaC5XaC+RQS4UZJLTbNAXbNAS8cL2jsENLoizPY6VKYyJMps+vz1tPYIOELVGBx5eGJVOKJlBIZqnqCtxZ0oJznTRmK6laGXnQwutjKz0018rhrnQBbB0UKSc1XEJ6vo9Wdg9eWis2VisOVic5cxOiFncdXE7r6bnd0AqbgWm6kdVXctqu5GZB1NdDbW0VpbTVNVBXUlRdSVFlBXmkdjRT4d9WWoJI30anqeINYzELGyMBXlaHuGs/2XHG3PcHm4woeHQz5+POX7b8/4cHfA66tNPro/5PtvT/jem2O++/aUn/7Le37+rx/xm3/7IX/+wy/48vc/4U9/+Fc++4/v8P79IitrVmbmZQxNNBIbKiMyUERytILUeBXhZCHRwRLRQX6ohGCyKA2w1S/Q6xOwBV6kAXYGMnD6XuDyZ+AJZuEL5+AJZ38LYF88B39CfP54dhEID2QTTeUSG8ohPpxLcjSfocliRqZLmV9qYGmthY0dCdv73ewc9LDz1PPuHMnZP1Wxf6pi70SZBnjrSMbOiai8cXhu4OjczPmVlYtrG9c3tm8B/Oadi8d3Tu7f2Lh53ZuG9/CVjoNbLTtXGjbPlWyeKtg+V7F7qWH3UsP+lZrDGy1HtzpO7gxc3Ju4fDD/d4DvTVzf67l6ZeDyVi+6GV5pODpTpAHe3pf8/wD8DHEdM0tVTL4sZ/JlJeNz5QxNFhMbyiGczCAYF/5HgJMjxQjBYAX98ToGhztZW3cwNqHDF2gjOaBFKi2goU5A1lVEXYVAR6OATVeFQ1/FoE9CV4OAWl1EtyKPtnaB6hoBlSofr7eFeLybWExKItFDINCOUp2P3dGIQlNEpywXlaEcqaKQcFRLn7OTujoBaVcOGmUxNlsdDnszXbI85JpyWrpyqO/IpK71CegWgc4uAae7kQ5pNvWtAq1dL+h11dAsEdBY8tDb8wgPNhMYqCE62kD/SD3R0QYSE82MzEsYWexhck3B6LKE2W058elawuPlxGdqSM7VPcnNNIgnd9EaYoMdJFISlldsrK072NxwsjBvJeCRYtQ0oequR9PTglraiaqrE5WkC3lHG531tbTWlNNYUUh9WT7NVcV0NVWjkrSil3UQC9oIe414+lQkI31sr05wfbrO3eUuH70+5Z+/c8fP/+UdP/3nN/zLd2/41+/d86N/uuOT793yyfdu+cWPv8OvfvpPfPbpj/jqy1/yu0//mR99cst3v7vFyWmceKqFQFScOAdiOQRiOcSGihmYrCQxXk58uIT4cAn9w8WEB4sJJAvwRMXy2eoXvvbz9T2FNwOnLxOvPxtvKBf38/DqGwAHknlPdpjPRlzZT+DmkRovYHiqmMnZcqbnq1hcbmJ1vY2tnS5293vSAO8eytg7knNwpv4WwGLmlbN3puLgQsfplZXzGzvXrxzc3Dl5defg7t7Jw6OT12/cvH7r5P6NnVePvVw9mDl7ZeDwRux5967VbF2o2DhTsX6qZPNczc7F17F3peXo1sDJ3RO8jxYuH8xc3Ju4ejBz/Wjm+s74LXjPr7WcXqo5OlOyfyxj97A7ffC/stHCy9XmdDwDPL9ay/RiJRPzZYzPlTM2W0ZqvID+VCbhZMa3MnA4kUVsMI/EcAEDo0UI0Wgt/lAFvkAVG9tORsZVDI9q8Hg7aGoS6Okpo7kxi8YagboKAWVHLlZ1GacbYTyWKlTyXJqaBRqbBJpbBfocNcjU2egtJWiMhSSHFcQHZVjsVbj9rXR2Z1HTINDekUl9vUCPooyenhKqqgQ06jKUsgJkPfnotOXI5IW0debQ0JJJbfMLymoFKusEGtsFFJoCQjEJfa4mapsFTH3VJEdVJMdVaK0FaO0F2ELlJKbbCQ6Lmdcayic51czwXCfzmzqWdk0s7xlZ3NWJvz7fweyagonFbubXtcyu6hmZUjE5b2RmrpdkUsH0dC8zUyYW5m1MjVuI+NVYDZ1o5U1oelrQ9HSgk0kxKGRouiWYlXI00na6mqpprSmluaqYluoSWmvKaastQ9JSgc0oxdOnIhqwsDQ7yMHWPBcHq7x/OOWT7z/y21/+kC9+91M++/RHfPbpj/j9rz7h03/7Pr/++T/xm3//Eb//9Kd8/vuf86cvfsZPfnTHxfkkq2tuhkclRAdq8Ibz8EezRWPooUL6U4XEh0tIjpaRHC0jPlxCZKiIUEoUbndHc7AFXmByC/T6vjbltnsz6PO8wOHNwOXPwhvKFgEOi32xrz+LQCyHUFKMYCIzXTInRp7hLWFytpzZhRpeLteztNLI2kYr27uiIN3u03PRNzPw3pkI8PaxPJ1998/VHF0ZuHzl4Prexd2jh7tHF/cPrq+z71sPD6/t3L3u4/rRwvmdkaNbsUTevVKxfalk60LF+qmctRMFG2diFt4+V7FzoWb3UsPhjZ6TOxPnj2YuXn8NcBroe9GO9OxWl86+JxcqDk8V/w3gpU0R4PmVJhHitcY0wFMLFYzPlaYBHhjLJzKQ8a0SOhjLIJLMJjYoPr8NjBYh2OyFWPsKiCdbiCZamZjW0J+UMjnrwOmR0tJWSEN9LlUlAnXlAh11AjZNOS+HdRysewl4W5F2Z9PZJR4mRAa76NK9QKIXaOgRUNpysQVr0PeV0SXLoqFREPWTywQqS55O9qoEOjtz6bM3o9dV0tb6ApWyBJOxjra2PKTSEnrkFZRXiwDXtgjIdAX06HKweWpR6HOQqjIZmFAzu2FH6yzGEq5CZsukL17O8Go3selG7LFCEjNNTK3KmF5VsHVs5/DCz/K6mYUVI7sHPg6Pwqyte1hcdLG2GmZ6ysHiQpCXM35GUr0ko3qiARUBVzdeuxSboROLpoNebRfyjlq03S2oJI2YVVIM8k4s6m6sWhlmlQSVpBFFRz0aaTPa7jaUnU2ouhpoqytGJ2sh4NAzPdTP/tocJ1uL3J/v8Z3HSz7/zU/582e/5NNf/gt//+tn/P2vn/HLX37C559/yhf/8Xv+9Pkf+MffPudX//4dLq+m2dj2MjYtpX+gCl84h2A0j0gyn2iqkORgMYmhMpLD5Qw8AZwYLSY6IkLsfwLYHsqkNyDQ68/AFshMl9F9T2+5zsDThNn/AldQwBvOIBDNJhTPJZzIoX8gj/6BPBKpAlIjhYxMlDIxU870fA3zC7UsrTSzstbM+nY7m7sd7ById76bB11sHHSxddTNzomM3VMlO+dPEJ+p2D9Xc3ip5fTWzPldL5f3dm5eu3j1xs3ta0d6ePX6nYeHtw7u39i5uu/l9FbP4Y2Wg1sNe7dqti7lrJ1KWTnqYuVQwtpRD5tnMrYvFGydy9m+ULB7peLoTs/Jg5HTRxNnr82cPZg4vTdycmfg9N7I+dOg6/hGw+GFkoNzBXunMnZPetg57mbrUMrG00rl0maLaNWz2sDcagPz6w3Mr9Yyt1qVLqHHZssYnCgkPpwrwpv2Rcr81gArMVIqTqF75AIOVxnJoXZiAy1s7LkZmdQRistYWotT35BHbXUOFU97vHVlAl1NAgl/G0dbAVx9dQSDHbR1CFTUC1gD1ShtuTQoBAzBUtTuIhrkAmWNAhJ5FnJZAXVVAvXlAk01AvW1Ai0tGXT35GM0iQcNrS0CankxZlM9SnklCkUl7Z2FSLqLqKgVqGkWqJcIKC1FBBOtuALVKAxZWNyV+Ac68A10oHIW4BttITEvIbXYRWyulchEA8MLEtaPetm/8LF36uPsIsrOro/tTT+H+1H2d+Isv/TzcsbP8nyMsUEXwwknIwknw3EH8WXzSScAACAASURBVKCZoEuFzy7HZ1fiNMtw96pwmOTY9N0YFG0Yle2YlJ1Y1F30aqToZW0YFR24zCpcZhUmZSe6nnYs6i6Mig6MynaMynb08lZiXisvxxLMDPWzMj3E4+URH7++5q+ff8p//e1zfvfpz/nii9/wt799wedf/J6//fXv/OnLP/K3v/wHP/nxI5fX48wt6ogkK3EGMghGxaWLaCKf+EAhA0NlDI5UMDxWxeB4BQOjJSTHSoiNlhIZKiIwUIg7mkNfOBNr8AW2YBa2YBb2kAjx89uuMyAuZLiDAp6QgC+SSTCWRTSRS3wgn0SqgORgIUOjpYyMl4sOBi9rebnYwOJyE8urLayut7G+3S7qOR9J2T6Ssnn4FCc9bJ3K2DlXsXuhZvdCLfr5Xmo5vtZzftfL1WMfN28cvHrn4v6dm7u3Lh7euHj9zsPr9y4e3jp49cbO5UMvJzdi5t2/UbN7o2L9spu1UynLB50s7Xewcihh47SHrXM5W+dydi6V7F2rObrTc3xv4PTRxOmjiZMHI8f3hnSc3IlT68MrFfsXCvbP5Gl4t4++hndlu43FjeY0vDNrDcyuNzC3UsPsSiVTCxVMzJcxMl1Carwg3f8+m5o9DwQjAwVpm9LESCmC2ZRLKNLA2JSMqTkVA6NStg9DXLyaRKkrp6klj/q6HGqrMmmuyaKxUqCxXCBkb+Ldq2nWl71MjZvokIgDpj5/Lb5UG12mLNTuIpT2QiS6TLr1uVgctcgVhdRUCOKfUy0gl5cgVxfTIc1G0pONtDubllaBLkk2CnkRamUlnZ351DUKyFVldCuLkOuLkaqz0VqLUOozMPUVotBlIlW9QO8sIzWrxZNswR6vJzTaSmi0mfFVJROrSvpHmple1nLxaoCbu2GOjuMcHcQ43EtysDvAxlI/s+NexlMuJga9jA/4SIbsJAN9xLy9OAzdmJVtmJVtaLrq6VVLCPTpCDkM9LvNaKVNyNtq6KwvQ9XZgFHejkUlwaqRYtVIsWm7cRjkuE0a3GYVPpsOeUctNn03EY8Zu16GwyBnbiTG+9sTVmZG2V6Z5Yffec1fvvwN//X3P/KXv3zGl1/+lv/9v/7Gl59/wT/+86989ZffcnGxwO5+PzsHLpY21MSHy4gk8wkncogkc4kNFjA4UsboRBVjk6IB9uB4GQPjpcTHyugfFl0ZPLFc7JEsbKEM7JEscakj/ARx4OmcLZiBO/TiCV6BQDSTcCLracBSwOBoEUNjxYxPVzA1V838UgOLq82srLWyut7G2kY765sdbOx0sL3fxe6JjN0TWbpM3jwRTbh3z7XsXeieZGINnNyYRHgfHNy+cXP33s3jxz5ef+Tj8YOXN++9vP3g4817Lw9vXLx6Y+fi3sLxtZbdKxW7V0p2rpWsX8pYO5WyuNfO4k4bK/udbBx3s30mZ/tMzs65gv0rdXoKfXpvTGfe5+n085PTwbWa/QsFe+dy9k5l7Bx3p4dX69vtosTORjPzqw3MrtSLRw2r9cys1TOzVMX0orhWOTpXxtB0MYmxPCKpLIKJF+n3X080A188S3SzGCkiNioe/Qt9tiJkCgGVRsDpLWNsVsHEnI4+bz17J8N0dBVQV5tBdYVAY00m9ZUCJbkCBnkx9+eTbCz50SjyUChyqW4Q6JQLGJyVmAI1KO3FNKsE5JZC3NF2oikFak0xjQ0CGnkxenU5VkcTarMoZtcpy8Rir8JoLkWnL0NvqEStKaO9PZum1gyq6wS6ZHlIlXmYHVVY3VUoNQIKtYDWkINcm0WHQkDVW8zyXoDxJTMz6xYGZnqYXNIyv9HL1KKR7cMQ948zXF2Nc7Cb4GhviMPdYTZXEyxMhZkYdDMUdZIM2Ym4Lfj7jETdokazSdGBRdWJwyBDK21Kh667GbdZicukwK7vQdlRT1djBZqupjS4Wmkz8rZadN0tmOQSjPJ2NNJmzOpOlJJ6JE3lGGRt9Ol6CDtNbMyPsTQ1zOxIgpvTXX72o+/x+e/+nf/6+x/5x9++5E9//Iyv/vpH/vjlf/DXP/+K733vmA8fL7G4pmdgtI7YUCnRVD6RpFjWRlP5DI2WMz5Vw8R0LaPTNQxOljM4WU5ivJzoiOjM4I3n4Yx+vY3l6M/GEc4SM/D/BbAvIhCICSK8T89Dg+MljE6WMTYllsxzi/Xpd96NrU42tyVsbkvY2ul6Mh/rZu9Uzv6ZQiyZTxRsnSq+BfDBlYGjGxNnt1Yu7+3pY4WHD37efFdcl3zzsZ93H4nx9oOP1+883L7u4/zOzOGlmt0LJTuXSrYu5Wyc97B60pUGeHmvg/UjKVunMrZOZWyfydm9UKYB/mbpLIKr4/iVnsMbDftXKvbO5eyefV06P7//rm21sbLRIsK7XMf0Uq24dbUsxtRSNZMvxeWN4ZkSUpOFxEdz0wD7ogLuJ4D9iWwiQwXEx0qIj5URHytDcDkrsDtL6XMUMbeoY3pRy8yigZEpDf5wJ1pDFc3NmdTXZtDelEdbfQ7F2aJtx+ywlY8flpBLMlAr8qisFuiSZ+AINOGItNOuzcXgrUfTV4kt0Eww0Y1CU0RDo4BBW4rb3YIn0oHOXkG7XMBgLycYb8dkL6NHlY1MkYNSVYykK4e29gxKy8RJd3uXwOCYhqOLBNOzGtRa8VlJqclCrsmlS5HN4Lie4WkD6wchlnd8LKy7WFr3sL3bz8nJMGenE5wejnO4O8rB1iibSynmJ0JMpQJMDgQZi/sZDHkIO6z4eo2kfG7GIkFCVjNekwa/RYdN1UWvWoJZ2YHDIMNhkOGzarCoOjHI2lB1NqCWNKKVNmNRSbBpu1FLGmmuLKCpvBBpUyUqSSPdrVVouprQ97Qia63BopIQcZlxGhWMJQKc7a7w6nyfi5Ntfv7j7/P3v37Gl3/4FV/9+TP+86s/8pc//pa//PnnvH23ztXtMCOTHeK1USznCeDs9PRycLyM0ekqxmaqGZ2uYni6kqGpCpITZcRGSwkNFuNL5OOK5eLoFw3f/m+A7cEXuEMv8Pa/IBgXCCdfiPCO5jE4UcToTBnjcxVMzFcyt1iffufd2JGwtdvD9p6M7T1ZeuK8f6zg4EzNwZmavTMNOycqtp+cFXbOdOxdiFKxx7e9nL+yc/3o5tVbHw8fgjy+D/L2OyHefTfM2++E0p/ffOzn8YOXm0cbZ69MHF5q2TlXiD3uhYL10x5WjrpY2u9gabc9nYE3T3q+Ub4rOH6l5/Te+D8CfHSr4+Baze6Fgt0zGTunPV/3vwddbO52pDWj51bqmX66BX4+Jxx9Wfmt9cmhqRISE4X0j4iuhL5EBu5vAjwgCgHGJ0qJT5QSGy9BGB2RodVkYjDlYHUUEorWsbHvJDEsYXhSjc1ZT2tHJm1tOXR1FtHdUUJtmbig79A38f7VIktTNmLhLjo7BVrbBLpV+UjUhfT624iNGwkOqrH5W1GZyqisFyirElCpcrFYKwimpDijzdjCtaRmVIy81OAI1qLrLUJvKUGmyqOt/QWVVeJudE2dgEKVQzIl4+ZulNOTMCPDUvqsZag1uRjN5QT7u1FqS3H5Opied7C5G2NzK8bKapid7RT7W6Nsrw1zuDXJ5tIwK3NJpoaCpCJOUiEnI/1+xmJhhiMBEj4Pvl4zKZ+XxZFhJmMRwjYLHqMal15OMmAn4beRCjlwGGT0qiXIWqtRSxpRtNelM+5zCa3rbkHeVouyvRFDTwd6WRt9hh4xKys6sOtlaLqaMMrbibjMeHs1+Pv0XB5s8sn33vLh4ZJf/PQH/O///PIJ5E/5X//4nC+/+BGra2GGRmWMTUuYWmglPlJENJVL/0AO/QM5xIfyGRwXd4+fAR6dFY2nBybLiY+ViCZmA4V4niD+NsAv0htZvv4scaiSyiI2lElyNI+hySLGZsuYfFnJzFIts8t1Ytm80ZZ+5909ULB7oGDvUMn+kYr9Y1Hb+fBcw8GZWhSoewJ491zP7rme/Uszh9e9nLzq4+LOwc1rL3fvAjx+FObxfYg3H4V5953+NMBvvxPizcd+Ht57ngZYRvavtOxeqNm5VLJ9oWD9pJvlg05W9sVYPZCkwX2OZ4C/WUI/w3t4oxWfo65U7JzL2T7pZvukm92THnZPep7K57a0zOzMUi2TL6uYeIqR+QqG58qZeFnF2JzoRZ2aKCI2lk9k+GstaFe/gKtfwB3LIJDKTQMcGy8hOlaMMJDoIjUgIdzfSCBSS3+ikdRIB/5oHcmRLlz+Rtq7BDo6s2htzqK1IYemavHWtbu1iK2XAa72U+gU+VSWCbS3vaBdkk2zJBuFsRqjqxlnpBu9vZ4miUBhuThJVhsKMDvKadcINKkEOo0CjmgN7kQ9Rk8RFk8pva5KVMZC2iQCVdUCHZ0vaG0XkPa8oEeegdVWxsy0moP9AEcHUeLxbuz2BhJJLU5nB3NzAba3UxwcjLK9OcjmygBnh/NcH6+xtTTBzFA/Y3EfI1EPqZCThL+PlN/JcNDHcNBHyuclYDHj0mnxG40Mer1MxSKMR/wM+pzEXBYmkkGGIi5CDgM2rRS7vgenUU6vugt5Wy3Spkr0Pa1Ynrx/DbI2XCYlwT4Tvl49PpsOt0WNUd6OrrsFXXcLDoOckE2PrLUGWWsN2u4WJlNhHq4O+Nfvv+FHP3zPv//8B/yff3zBP/72OX/507/z2Wc/4NXdHHMLBlKjTeKG1VABkVQO/YPZxIZzSY597XQ/MlPB6GwlY3NVjM5WpgXVIiPFBFL5eJP5uOO5OKPZOPrFfWh7KENcp+zPJJTIo38wn8RYHgMTOQxPFTI+V8r0YiVzKzW8XGtgYb2R1Z12NvYl7ByI1if7R6KG8+GJhqNT7ddfz3Xsn2rYOdawfSTK4+yeG9m7MHFw2cvxTR+nrxxcPbi5fR3g/l34vwP8cYQ3Hwd583GQ+w8ebt86ubi3pAHeu9Swd6lh+1zF2lEPS3uSNMBrh11snvR8qwfeu1SlN7KeM+/hjZaDa3HRY/9K7H13TnvYPOoSHSOOu8Xp814nq5strGy0sLDSwNRCNRPzlWmAx+YqGJ0tF8Xen474n61VQoM5+JLZeOKZOCICjogIsH8gh/BoIf3jxUTGigiNFCCEAk2YjHm4vZX0OYrwhaqYnFXQn2omNtTGyJQKQ28pGm0Jbe0Z1NcKtDflU1eRRU9rCZvzfn7+/T281gYkrS8waitpa8tCZ2qgR11Bl7ociaaUxq5MmrszaOwUp9UdPQL6viIU9jzajQLtegGdtxBzoARzoAR3vA6bv4JubTYSRQY9ylw6JAJqbT4KVTYabQ5dUgGDPodIsJmdrSDHBym212OkkmbGRpyMDrs42Jvi5nyZh5tNzvZfsrk4ztrMGDODCfpdfQRsRiIuMwMBF6PRABPRMKPhAANeF1GHjX57Hwm3A79JT59SRszRy3yqn8lokJBNT9xtYXkqxerMEGNxHyZFO2ZlBxaVBJdJSXdzFd3NVagljfTpevBY1LhMShx6JW6TBodRgb9PT9BuYHowwsuxBKmQk4mYn/mhKH26HoJ2A/1uC2OJAG9vjvnlj7/Hj3/wjl//4of813/+ga/+8ms+++z73D/OMTWrYXKui7HZRhKjxURSOUSHckiM5pOaKPoGwGWMzpanAR6crkwDHBwswJvMx5PIwxnNFiMiQuyKZOCNZYtvySMFDE4VMDSdz9hcEVOLZcyt1LCwXs/SZjPLWy2iBOxxDwcnSjHTnmjS0J6c6zk61XJ8puPgTMv+6RO8R6JA3d6Fif0LC4dXVk5u7Zzfu7h+8PHqTZCH9xFefxTl8X2Itx/38+47/bz5KMzrjwI8fvDy6p2Lq0cr53dmTm4M7F9p07FzrmL9qIelXRHe5b0O1g670ll390LJ3qWKg2sNB9ea9EaWWDJr2LtUfR3ncraOpWwcSkTPpqdp+sZOOysbzSytNTG/VMfkyyrG5yrSWfi5bB6eq0ybesfGCtOWos9a0PawgD0s4Iq+wJfMJjRSkIY3MJSHEA42YzIU4HRV0msrRGN4wfS8CpuzkJEJKdMLesZmNHh8TTQ2CbS0ZCHtLKO6IhOnpYuYW8qrwyFeX0zTVC2gU5RjNjai1tZhdXWhMNZTVi9QUifQ2CUC3KEQxI2p3nx6I9WoPQUo7NlovXn0xSuJT3cyMNeDP9WEVJOBRPGCVqmAobcMo7Wczh4BhToTjTaLLolAW5OAsiebyVErS/MhluaiLM3FuDpZ4e5ym1dnm1wdrXO4Ps/CWIqxSJjx/n7mBwcZjwZIeKwEerUErToSnj6GQx5Gwl6Ggm6mYhHCViMevYpBnx1zdxtWRQcbU8Ok/H2MhF1PLoIupgfDLE4k8du0mBQdmBQd6LpbUEsakbfVYlJ0ELQbcJtVWNU9BGxGnCYlRkUHfpsOt1nFYMDBRMzPUNDJUNDJ/FCU8USArYVxtpcnWZsf4fXtIb/62ff5yScf+Oovv+XvX/2Gf/u3Nzw8LvByycTsoozUeDXRoQJiw/kkx4pITRQxNFVCarqEwZlShufKGH06KB+bq/haymWsmOBgHp5EDs5oJo7+LJzRbFzRTDzxbPyJXMKDhSTHShicLGV4pojxlyXMrFQyv17DwmYDS9tNrO+J9p87xz3snsjYfyqTT871/y2OTrXsnajZOVKyfaRm90QrZt4ryzcAdnJ+7+H6IcDt6xD37/p5/BDj9Yd+3n78BO+HEA8f/Dx89A2AH3s5utWxd6kSs+qZit1TJRsH3SzudPw/4X0eVD2XzwfXGnbOFWIvfSYXf89pD9sn3WweiOobW3ud6f53dbOFxdVG5pfqmHlZzeTLKqYWqtO98Ph8ZVrs/dkfuH+4kEAyB3csA2e/QF9IwNmfIf7bD+ThT+XjT+WLLhoDeQh91jKc9kpU6iy8vlrM1gLM1gJ8oWqGx6WsbNtIDEuIJSXYnfW4PR1IpSU0NeZj1rWglRSwNungh282UXXmIW3LwWxsxOWRoTM1UdUgkFMqHu1LtcV0a/JolwlUNwmoLXlI9C+QWjLxDDYSHG9B683BFisjONKEqi8Ho7sMlbUQqSYLo6MCY185bd0CnTIBqUxAKc9ELn2Bsjsfi66WoUQf26tjXJ1scH26yfXRJttLU8wM9TMeDzEV72cqHmc8EmMsEiHuthGxGwladYRseiJ2IzGXhbjbRsJjZTTkZioWYDYZZmk0TsiqpVfeznDQwUjIyXjUS9xnTT8zTaVC7CxN4u3VoOlqwqzsxKzsxK6X4TIp6dP10KvuwqruwaaR4TQpMSk7sWm7MSk68Fs06e8j3Geg32FiOhliOhlka2GcqVSIV2fbfP/dNX/+wy/46x8/5R9//x2f/+ETHl8vs7zWx8SslIm5VsZm69KKF6mJEgYni58ALmFotpSR+fL/BnD/aBGBVC7ueDbOaCauWA6uWA7eRA6+ZC6hVAHRkSIGJ8sZmalgbL6cqaVy5tfrWNxqZHmnmdW9VjYPxGy0eyJj71RciTw813B6YfhWnF0aOTnXs3+qYfdYlQZ499zI/qX5/wFwjMcPMR7fR3j94Wt47z94uP/g4vatncsHcfHi8EbL7oVSnCw/Tbk3DrpZ2elkfV/C+pE0PXXeu1R9aw/66FaXHl49A/wM78ax2PduHUvTAG/vS9IAr2yIhmfzS3XMLtQwu1CTng08i9sNz30N8bfE7J4Atoe/BtiXzH0C9+sKSTDqC+kPt6NUZdNrLcfSW4LBmMfwWA+BSC1DY13MLeroH2jDZCunokagqTWD1vZCes3taCUFeAx13B2OM5UyI+/Mx2JowGJppbk9j9JqgYa2LNp78lEZK1AZSulSZFPTLKC2FCHRZtChFXDGahleVOIdbkDvz0PvL0DlzEXVl4dEl0mbQkBjK8XoqKBL/QKpWkCmyUSpzMNubWAgpiMVM7O9OsHZwQpn++vMT6SYHUkw1O8j5ukj5uljIOBhwO8n4fLSb7fjNmnw27SEHAZCDgNBq5ZA7zNEBlL+PgYDdgb9fUzGPLwcCjMSspNwmxgJ2ZkbjLA5O0LUacJnVjEe9bI0lmBhJI5Z1k6vohN9VzPG7laCVh0pvx2vSYVZ1olDr8SskmBUdGCQtYnAy9pRtdVh6m5B015Hn0qCz6yiV9GBt1fN/GiMo62X3F9s86uffZ/Pf/8L/vKnX/PlFz/m5naexZU+EkPNDE82kRgpJzYq9lbJyZK0Xck3AX7uxQZnRGOtyEgh/oEcXLEsHP0ZuGJZuJ/+8zyL0MXHShieFvvnycUq5tZqWdoWnQPXDzpF797jHnZPxVXIgzM1Rxdaji91nFwZ03F8aeD82sDppVhCPwP87LLw7RLayfm9j6v7ILevI2mAH96JffDDhyB377zcvXdz+8HJ1VsrZw9GTu8NHFyr2T6TsXkibndtH4vaWqLWlvi97p0pOLhQcXip5uhKw/G1lpMbHcfXWs5eGTh7ZeDoSsP+uZLdUzlbR91sHHSle9/NAwkb+51s70vY3pewudPOynqTaDW6KLoVvlyu5+VaIy/XGtMCd88/QMfmqhieLic+XkRoMAdPNANnWMAREkXsXP3ZeGK5eON5eOP5uKO5uPpzECymEoL+FtzuBhSKbNTqPHqt5cSSEnzBWix9+QwMP9lk+GtY3fJjsNZS1/QChaIcrbSY1nKBmaSJj+7WUEgKUMvLqasTDcw6u4vwRuRYnK1IFAW0SbPoUuSmTcxMzkr0zjKUvTk4Y7WMLCmwJ6roNIk9sbwvlzatQIv6BSpHGTpnGR2aFzTLxFK8Q5qJSllG0K9idjzM6f4yR9vLbC7NMj2cIuS04u8z4+vV47caCPZZCPZZ8VksuE0GnAYVgT4d/W4zEZeJkE1H0Kol3CcCPNrvJBWwMhp2MBzsYyziJO4y4tBIGPBa2ZwdYXdhgvGoF7dBztxghJjLzMbMCCGbHrOsPV0Se00qxqM+ZlMRvCYNOkkLBlkbvRopZmUnJkUHfWop2s5G/GYVHoMcm7KTqMNIv8NIzGVmZWKA9bkR7k43+dknH/jsN7/gqz//nr/8+Rd897sHHBzFmXmpYXymk8RwFfExcTiVnCh7srIsFgGeK2Z4voSx+XJGZspITZcQHy8gPCwanDmjmdgjL56ycNYTwLlEhgpIjItDsPH5atG3d7OJ1V3R9nPrqPtreM+VHJ5rvgGvntNrUzpOrvTfAnjvRIR390T7PwJ8dufl6j7IzWOYu7dRHt5HuX8b4uFdgLt3Ytl8897Bzfs+rt72cvao5/RBx8GNku3zbjafJsXbx+Ku9eZ+d/r7fQb4m/Ce3uo5udGlAT6+1nJwoUoDvLbXycahRPx7H4oQPwO8sd3G8lrjN/yC61laa2J5q5XlrVYWnrayJheqn3riakZmKkhOlhAeysUT/dpKxR58gTOciTOSjTuaizuaj6s/D2ckF0GvLcBiKiUa7kKjLEQiyaCtTcBsKSU5IMEfriEx1MzAWCfzq0bcoUbGZ3sZmrBgNNXQq66moVAgaGnj8eIlPe251FWJPkoV1QJaQw2DY1aszjaaO7KobxHlc2obBboVeZjslfij7ej7ipCbs/APNeEZqEdiEmjRCHQYBRqUAu36bDTuSvTeSiTGbBoVAp3aLCz2JqzWdnxeLUvzw6wtTTE1OojfbsNpNhK023AYNRgVEgzydmw6OZ5eHZ5eA06DBq9FR9BuEOF9ysIRl4l+t5mw08jMQIiEp5eJmI/hkJOo00S4T49LLyPutrC/NMVUIsD+0hQ+s4qZgRBTiQAjYTfjUR99ailWpQS3QUHMZaHfYcIsa0fb2YxdI25d+W06XCaluPCh6kLT0UDYpqPfbsBvVjEZ8zHgtbA6lWQq6WN3eYKTrXluT7b56Sc/4E9f/J6//fVTfvvbj7l7mGV7z8P4fDeDU00kx6tJTlQQnyglMVlCcrqY1GwJqdkihuaKGZ0rY3hGzM6xsXxCQ7n4kpm44hk4ogLO2AvciUz8A6IQe3REdFB41m+eX2sWXQP3pU8wyJ9O/VQcXGi+Aa6BsxtjOk6vDWJc6ji5+MbF0YnodbR7Lg6x9s7NT1NoB6evXFze+Z7K6Ah3byPcvw2kAb596+T6nY2b9zYu35k5fa3l+EHNwSsFu5fdbJ1J2TkVn3n2jnrYPur+1g+cZ4C/Ce8zwKe3+jTA++dKto97WN8Xs+7mgYStwy429jvZ2utkc7eD9a1WltcaWVxuYHG5gaW1JlY3W1nb7WB9rzOtF/2skfU8mR6cLCU2nI8/lokzIIrYfW2pIuqOucL5OEN5OIK5CF2dAmZDGW5HE4qefFpbBOrrBczmCry+egKRWuKDTcyv6phd1bO8bcfsqkBnrSCaVGPX19NULODQ1fPuZpnDrUEaawUKC0Q7FbmqDH9IjlJXSWdPAW1dudQ2iQf8XT35qHQlGK3l9Hlr6PVUoHPmo3XlIbNl0qgWqFUIlHcJtBtzUHsq0fgqkPTm0qQWkPUW096Tj9OtYGwkyM7WPLMTIwwnY/j77ASddmxaNVatLL13bFZJ6NPLcBhVOIwqvBYdfpsYPquGoF1PzNtLzNtL2GlkrN9DuE/PeNTLZDzAcMjFRMzPaMTDeNTH1twoe4uTXGwvMRJ2EbLpGAzYSXptvByOMRhwoO9qxqWXk/BYsSoleIxKhoJuvCYNNm03HouaiMuM36Yj0KvFLGvHqe3GpuxkOhFgfjDCZMzD6fosZ1vzfPzqiB9+uOHubJeP3jzwu1//kr9/9Xv+/rdf8u79Kps7HpKjbSRGGxiYqCM5UUV8oly08fwGwKm5AkZmSxmaFv14o6N5aYDdicw0vN4B0eS7f1QsxYfnKplarEsrSK7sdLJxIEuf+j0DfHipTYP6TXifAT650nNyoeb4XLz53T1WpAHeOdOJ78BnYhY+uraLZfSt5wniELevQ9y98XP/nH3f9HH11sr1ewuX742cvlZz/KDk4JWMvasets+72D2RJ3J3YAAAIABJREFUfhvgp93r7eMe9s+V/yPAz5+PrjQcXKg4uFCxcyJj46ArPazbOhQ/b+y0s7HTzvpWKyvrTaysiUcbKxstrG+LT2qbB12sPx35P8vsPJfTw9PlJEYLCSaycYde4PCLcrK9HtHdsc+fjSOQj92fh82bg1BfK6BR5mPQliNpy6S9WaBbko3FUo3eUEg02czsoor5NQ1j890MTXczvWKk11OD3lzJYFiDQVJKS4XAeMLEd99u01wv6l51dGbR0ZVDb18TXT2FSGVFtEvyqawRKCsT6OzMR2+oRK7MRW8poc9bg9ZeQI/5BTJbFlJrBq0GgVq1QI+zFE2ghm5HEQ1agQqpgMSUT6+zg9GJIKtrk0xPDxLyufE47PgcDoJuNwZFD706GX1GOb16KWatBIuuC6uhm15dN3aDEqdJ/RRKfDYdUa+VmNdK0G4g7rMxEvWyMj3E4vggY/1+hoJuRsJephJhlscHeHO+x+nGSzZmhul3GLFrpAwGHEwnQxwsz2CWtaNsrSXQq8Wll9Or6MRtUGHXyDApOrBpu8UFkqiX+aEoY/1e4i4zAYsan15OrM/ASMjO+tQA59svudhZ4Gffv+dfPrrn+x8+8O8//xn/+dUf+D//+3f82y9uOT6LMzojIznWxOBUEwMTNU8Al5KcKmVgppjUbBEDs/kMz5SI+7cThWmA/QNZeJJZuBOZeJJZ+AdziYwUEp8QvYvGFkT515frLSxsdbK8K2VjX8HmoZytIxHC/XMtR0+7y6e3Zs5ujJzfmji/Nf3PAJ89yeYcK79hVqZl59TI3rmZw6s+jq7tnN24Ob/1PJXSQW4fPbx64+bmjYOrRysXry1cvTNz/lbPyaOKo3tFGuCdCym7J9KnVceny6d9Cev7EjYPpeyeyjm8VKfB/WY8w7t/rkz3wZuH0jTA2/sSEdztNjG2WlndaGZto5W1jVZWN1vZ2BGvrnYPv75SEjWjnyBerGZ8roLB8RL6B/Lw94u6YnafgNUlYHW9oM+Tjd2Ti82VTa8jE6GqRKC+SkCnLEUhzae9OYPOthdIJBnYHdX4AjUkh9rY2reSmmgjMdbGwoaF6EgnekspdeUCZlkdVfkCXks7//zxAcruAqorRDfD9o4MbI4WUduqI5vqWoHSEoGyUoGmhmxs1mbsfU3Ilbm0SgW01gL07kK6zAIdJoFGjUCNSkDhKcMQqkHhLKPdmEOT5gVGXxPJYQeTU3EWFsYZGogwNjTASGqAsVSKiN9DyGXHYVajlbWikjZh1kpwWcXrIaOig16NlD69ArtBid2gxGvVEvX0EvX0ErQbGO73MJEMMhh2MRrxsTSWYmksxUjYS9TZy+Jo4v/r67y+28yPNP0pS6RISZSYA3LOOZEgmBOYM5gjAgEQkTkpdXAnd3AHu93TDu20Mz32zPic3b3bv+zZiw+EWmPvXtQ50AUlHh4+qvpVvfUW+5uLnKZ3yG4vEVuaKpXZDsa6fCyGe5nsbcellWKSNtDjNjPgt9Nu0dDlNDLWG6C/3cH8aC/TQyFWZ0ZYHh8oN8WCVg19HjM2RQPrM4PkdiO8e7rPF++d8M0nr/n0/Xf49KOf8ecfvuU//vobfvzxC168WmMn5mMqIhMBjtSKJytLGXhipZrJ1WoR5OUqppYevgVw3+Sd8l3a7rHb4tt3trJ8PSGyWc9atKV0NUHFbkZDIqcrA5w8MLF/bC/LH4uXbo6uPBxducT4CcDFMwuFUzPZI0OpwaQv3zraK5pJHznYP3aSO/VSOPdzeN7B8eV1Kd3DxUtxjfD8tZ/Tl26OX7k5fdfF0Ts2Dl6ZyF3pyF5qywCnD9WkCiJw8YyCWCkSORXpA91bABfPreXInZjIHBnIHBneAvi6hL4GOJaUlGM3Ia5LxpJSYklpeW0yU9Cwn9eUvy6abmMz3szKdh2L60+ZiTwmPFVBX/guoYHbdPTewNcphj90uwTvHVz+mwhPbwvo2+4z3Kkj3GdAKbmJ0/YYleoGnaE6OoM1hEea2diwsLCkJjzZzOyymtE5GVK1gKT5Ji1Pb6JouItTV8vzw1Vmwxb0yts8eyQ6dpgtj2iRiMv7SkU1D+4KPKkUeFwhoFVU4XTU0t7Rit39BKPrPq6exzj7HqH0CEhdAkrfDfTBB5hDjzF1PMbc8YzAgIyhKQcrq2NsrM6zvjLH8vw0C9PjTI8NM9rXTV+Hn6DLSrvDTIfTRJfXRl+7iy6vDZ9Zi1XdiknRhEXVgsekpNtrYTjkYTTkZbjDxVC7k6F2B2NdXnHm29/J1swo65PD7MyNszDUzaDfxtrkICfJLYrRVXYXJxgNeZju76DfZyUy2ksxtobfqsapl6KT1NJu12JWNuG3ahgNeem06xkMupjo78Bv1TDY6Wa420PQbWA45MZvVdPtNdNh1RBdGucqF+W7n7/m4xcHfPHhcz77+AV///H3/Ndff89//vVf+PrLczKZMCsbFhbXZOLBrEglE8sVpfL5EVOrdcysNzC18ojxSCWjCxUlGd/9soyvZ+wmveO3GJy+x+jCT8zXdxrYSohvXxEAFbG8hnheT/LAROrYxv6Ji8ypm/yFj8Kln4MrHwdXHopXTg4uxI5u/tT8VlMoUdQQL2iJ5tTEcjoSBQP7B1ayR3byJ26KZ16KZz4OLwKcPA9y/irExasg5y8DnL0SzepOXns4fu3g4JWFwgsD2Ust++dq9k81YgY+0pA6UJXfrLGsnN2snFheSfJQS/bMXJr/OilcOiic2Sic2cifWsmfWsmdWMgcmUgVteLaY05FKqv6ydqghN2UhGhaSmxfRiKrIJ6RE9uXsZeRks6pyBXV5A905A/1ZPLq0sipjY2dRpY26pldeiKKcCYq6Bq8I65wdt3CH7qJr/MW7nYBl1fA7RMQ6u4IGFseMhIy0hdUopLeQi4RUCgE2jueYrXcoruzhqkJKYODdXR2VzE61cJUREbXQD1G7SNUbQ+pvivg1NWSWO3nt1+d0lYrIG0WMOkfIJEKNDQKGMxPkUkf8vCBQNOz+zytFKipElAq7uF21WNzicorg+ce5vZ7qNwCDQaBVquAxncHW1cN3r4m2gfl9E+YmZzrZHK8h8WZCeanxhju66LT76bD6yTkcxD02Bjs9NMbcBPy2Ol02+j2OOjxOulyW+l0mgk6DXQ49HS6jPQF7Ax3uMoR7vQQtGnpdhnp91oY9NvE2e9wF6m1OVJrc6TX51kZ72dlvJ/k6iznmSip9Xl2FsYJd7pZmxoiv7vCeF+gvMHkNStx6CR0e82EOz30ea0MBl14zSpCHjN97Q4Gu90o22pQND+m3aljMOgk3O1lZbyX3PYirwsJPn99wre/eM1//fi9aCX7x1/zHz/+lpOjLXZ3+tlLBZlebGZ8/iEj83cYXbzD+HIFE8uPmVyuZ2qloaQCEuEdmbvP4PQ9esdv0Td6U3TUmLwrKq6uT59s1bERa2Yn2UY0LSW6LyeWURDNqYnn9ewdWkgd20ifusmce8ld+slf+Slc+ilceSlcuMhf2N/Ae2Jk70BLvKAmllcRzanZzaqIZrUkSuV45shC7thB/sRJ4dTDwbmP46t2zl52cP6yg7MXfk5fet8A/MrFwUsbxZdmMhe6/yfA8ZyCWFZKNC8jXlC9BXDh0kHh0kHx3EnhzFGGOHdiYf9QbGIl82rSOXW5HI9nFOzuS9lJS9jdlxLNyEjklcRzCuIZKYmsjP2CkvyBhsKhluKRjmxBQyqjEFcOo80sbzYwv/KU8bnHDE9W0lkC2B+6iT90E2/wJq6AgNMjhlB3V0BScwOz4hFucz065T2krQJyuYDdUUnA95j2QBWhzkeMjDQSHm9iaKyB8EwzSxs2XM46Gp4INNUISGsFBgIK/vLdK0KeJmRNAk5bDQrlHdrabqIzPKWl+S4NtbcxqMQTJA/vieW0Qn4bq/MJJmcFKtsNVA5RH91sFJA7BMzBKgIjbfRP6QnPOZiOBFhY6md0uIup0SFGB3sJBTz4nFY6vE76O30MhPx0um34bUacBjUOvQq3QUPAaqTTaabLbSXkNhF0Ggg6DXSX5r/DHS5GQ14me9sZDNjp81oIOfQEbVqGAnYGfNZyc+l4b5O1yUHCQRdTvQE2Z8NszY2ytzrL2tQQm7Nh4svTrEwPEXIbMcobMCkacRlkovBjrJ+FkR46HHoMsgZcRgV97Q5mxrrx2FSYVE30Bx30+Cz0+CxEwl3ElyZILU9xmd3ly4+f89tvf87f/vI9/+d//5Uf//Id2fQSu9EBtnY9zC61Mrn4mNHFe4xF7jK+XMH40iPGFp8xHqllcvkxowsVDM/eY2jmLgNTd+kdF90fBifuMTZXxeTiY+ZWxcbVerSJrUQr0bRMbMSUAI7lNSQKBvYOLaRP7Oyfeche+Mhd+sldilk4f+khfy7qkrPHRjJHBpKHOuIFdVmTvJNRsr2vIJbRvPWmzhza3srExxcBTp8HOHvRzulzHycvSnY3JYDFe0dmsudv3r+pY5VYQv8E4EReTqwgJ1FUkzrSlVVYxXN7KZylsJM/tZI9NotikBLAmcKbhlgiqySakbG7Ly0DXJZZZmXs5eRkiipyhxqKRzqKRzpyRS3prJJYUsp2rIXV7SYW12qZXKhhdKaa7pH7BAdu0959m0DXLXydt94GuL5SoK3mJm11t/A5mnHZnmHU3UejuYXLXY3bU0VH52M6ux4zOS1jal5G/2g9wYFq5paNDA6q0aurqLwj0Fon0Olo5PXxGuFuDT57HRZDNVpNJU1NAk9rBZ48FmhpvodBW0/jsxvolNXUPhNoaRZw++vpHpLg73mGraMSU+AuLfprgB/RPtRGz6ie/nErwxNuwuPthIdC9IXa6fA68btsYvYNeOjtEKPDacFr0eMyavCYdXQ6rfT53Qx2eBjs8BBym2i36wjYtITcJgYDjjK8MwOdzA2FGAm66HIaCDn0jHa66XWbGOlw0us2sTzWx/bcKNtzo6xODDA/3MViuIdoZJLVyUHSGwskVmaIrkwzHHKjk9Sil9Zh07Qy0uUhMtrH9vw4nS4jC2N9uE1KRnsDjA220+Ex0B900NdhJ+Q24jUrWZnsY3NmiOhcmO2FMGeFOEfZKP/+p+/5P//7P/jXP33L84s91tZDzC7omF5sFR03Fu4yFrnLxEol48tVjEVqGIuI6p/w/AMGp+8wMHWb/sk7Ytk8cY+R6QomFx8zu/KMyGYjq7vN4umTlJTYvlwsGzMlNVNez17RWC6f38DrJ38VKAOcO3OISwVHYic3XlCzk5GzlZaWQs5mSsZuWvXWuzpdtJA5tJE7dlE49XB07ufk6jq8HD93c/LCw/Er908AtpI505az7z8DeK+gIF5UiDPiI11ZjZU/tVI4s/0DwOL3bSBV0JAqaMgd6MkWdWWgY1k50YyMaEZGLCsXVVoFFXs5Ocm84p8CvJ8TPbN24uIx8MWNOqYjzxibfSQ6hQ7fo7PvDh09dwh03cHTcQO3r1RCP74nUFcloGytwG1tIuiXIGsTUKtv4/E8QacXCAQfEex+TP9oI91DT+kbqyc48JjB8Rbie/2YDQ9prhOoqRSQ1Qoc7k3y/HCZytsCWnkFamUVNTUCFQ8FmhpvI2l7gLS1AmnbAxRt93nyWGxstckFrJ5qfN21WAKVtBkF2owCOvd93D0NdA4p6Ro20DNkoX/IyeCQj5FBsWz2Oa2EAh56gn7aPQ78DhMdbisesw67TolZJcWmVRCwGktltJ2Qy0KX24rHqMaibMOqkuA1qwg6DfR5rQz47YwE3fT7bPS4TQz4bcwMBAl3upnqDTAUsDPT38HWbJid+TG2ZsPMD3exNNbH1twoi+EeDuLr5HaWycVW2VwYK+8P66V1+CwqpodCrM+NMhzycJjaJugysjgxwNRwJ16rim6/hd52GzPDIWZHutiYHWJrfoTc9iLZ7SVenmT52etL/vz77/jxL7/hf/39z3z95XOisUHWt5wMT9UxMHmfgalbjMzfYmzpAePLVYwvPWFi+RnjkWpG5ioYnL7HwNRd+ifu0T9xr2x8N7NUy8J6PcvbzeLM9yenTxIZNYmSh1Wi1LxKHdvInLrLAOevAv8A8P6JWVzaL2jYzSrYTElY3xPj+irhTkpJPKt9A3HeRLpoIXdkp3jq4ujcz/GlrxRig0yEWLymcH2wLHOmZ/9UR+ZYy/6Rhv0jTbmRlcjLSRaVJA6U7B1oSB6K82CxUWUmd2IpQWx/A/ChnnRRWwY4c6gnc6hn/0BHMq8uleXysirtpwD/NAOXS+iiuDt8DfD6bguRzXpmlmoZW3jC0NQjesIP6By8R3ufmIm9QQFPQAyh9pFAxR0BRVslirb7hAdNqOS3MBge4HI9Yigsw+GrRGcR6B9rpm+skYlFOYOTTQxNtDI+qWNhwcletJemZwI1DwRmBs1cFpcxKqtQSyuQtN6n6qHAk6cCGvUTmhpvU1UpIGm9S2ujgEx6G5n8Fq1tJUOA3gZCQ03o3XdR2W5g76ihc0jJ4LiZwTEnQ2EPQ+EAw8NBggEnXocFn9NKV7uXUMCD22bCazMQ8jnoa/cQdIkgey36cgbuD7jo9Tnocltx6hRoW+vRtNRhVjbjNavodpkY8NvpchoZDDgYCboYancw0eNnuMPJ4kg3swNB1qeGWJscZH4oxPrUEAsj3SyP97O7OFGO1Po86e0I+ztLjPX66W+3Y1I0opPU0u21MNrjZ6w3QHxtjvmxPvKJDYqpTcYH2pkd7WYzMs7+zhIvj/dJb85xmY/ywXmObz58ya+++JDfffdL/vLDb/j73/5UBvjlq02WVi1MRpoZna9mZO4e4YW7jEYqGI1UEl6oFsUdi1UMzz5gcPqeGFMPGJx6wOhMNRPzT5hfbSCy0czarpTNhIzdpJJoSkEsrRQhzomnPxMFg5iBjxxiA+vcS/bCVwY4f+Eje+4ic+IgdWgmWRDnqNv7Mtb3WlmNi7ERb2Mj3vYWwImcjr2ckXTp+sLBmfsnAHvKAIsQOzl+7qR4ZSN/aXoL4GuIrwHeKyhIFpXsHYrv371rlVZR+5YY5TobZ4/Nou1PCd50UfsWwKmCpgywWJ4rxUWHf5KB/zvAibS8dAi8laWtBmZW6xlfrGFoupre0YoywIGuW3g6fgKwzdxKQ+1NpK0VqGWVTIQddAbasJoe4vM9ZXHJgdl5F6VRYGRWRu9YE5FtC5EtM6OzcvayPUSWnSzOW5kc0dFWK7AQtnOUnGM+7KXl2Q2eVgs8rBDHSlJ5BXUNosijtlbg6ROxfFYo7yCVCbTKBQz2+3iCNdgD1Wisd7D7n9DZr6R/2MLAsIuBQS/d3W6CATsOsx6rQYNFr8Zm1OIw63FZjfgdJoIeGx1OC36bEbdJi9OgxqVX4zXpyssEXW4rPrMWh1aOQyvHZ9bS5RZHSdNDIYY7XMwMdDI7GGIk6Ga828doyMNKqVzenBkhsTxNfGmKo8RGWdhRjK1RiK6yORtmb3WW6Mo00RXxLTzeF6DTZcCiEpf+PSYlwyEPc+Ee8vF1zvMJznIxdpenOEpv8+IozUUhwS8+eM4nLw/5l8/f5ZuPnvPDrz7ll59/xC9+/iF/+uF7/uff/5U//eErPv74gPc/2GVmQc3ChpzplWeMRyoZi9xnbKmS8GIFI/OVjMxXlXdQh6YrGJ6pZGS2mpHZasbnapharGVxvYXlrVbWozK2kwqiKRWxtJr4vkqM0o2ieF4vQnxgI3XkIHXiIl3KxNkLH9kzD5kzJ+kjG4mCgXhWTXRfyWZKJsK728zqbjNr0Za3AN7L60WRSE68wlA4cXJ47nkr+17Pl48vXW90y9deVWcmMqdGsicixNkjLZlDDemiimRRSepAxd6hSiyri29M9d6Yx5cgPraQPTSVS2VxUUNb9vJKH+hIlgCO5xREcwpieWV5WymZV5AqKMkeqMkfad+U0Aei9ey1OmszIWFlp4m59UYml54xPPOIvrFKOgfviVcfrzNwhxhCe4cBnaGO1pZ7mI21uGwNhNolNNYJOJ2PMVnuobfdwRN6Qv9EGxqbwNKOjb18iKHJNkKD9WxH/aytOHFbHyKpE3DpHnGUnCO6PEj9I9E+tqHuBo0Nt5DIHmCy1qI1iF5XTU2iraxEegOT+SFqwy0kSgG97R6B7gasnmr8oRZ6h3QMjToJjwYYHAoQ6nDjspvo7vDR4XViN+kwahS4rEa6O3z0d/oIesQGljhGstDuMOMz68sAB6x6nDoFDq0cp06B26DCa9IQsOrLI6WZgU6WxvpZDPcy3u1nMdzDysRAuUmVXJ3lJLlFditCfmeZaGSSs/1d3jnOcJmLc7S3yWl6h+TmAivTQ+wsTTI50MHMcIhOl4GATUvApsVv1eA2KlibDbM+N8rG/ChH6W1OMru8Osnw+jTLFz+74q+//5o///pTvnjvjC8/fMHnH77DF598wN9+/CP/9j++48d/+4Zvv73i6kWEg5MBppZaGJ2vZnj2ntiJjlQwtvRQFHQs15QBFuF9Y5Y2Mf+UmaV6IhutrGxL2Ywr2EmqiaXVZYATmTdXAqNZLfG8nnjBwt6Bjb0jB8ljJ5lzrxinbtIndpIHFmI5HdF9JTspORtJKavxVlZ2mv6fAO/lxQNmmQMrhRMnRxfeMrwnV95/APjaQjZ7ZvoJwAayJzpyxzqyR1r2D0rz4LcAFnd6RWGHllRR3F7KHpvLAKcKGnF09N8Avh4rXXedY3kl8YKqbLWTKihJX2ffYx0Hx3oOjvXkDw1kCtqSqEPG1p6UtWgL8xsNTK/UEZ57Qv9EJV3D98Tzrb13S+MksTMt6MwNeANqKqsEBvrMBFytuG3P0CruYDbcp0ki0N7TiLfrGcNzarxdNfh6nrC662IqoiPYX4s3WM3GppfZSQO1FQKqeoHd+W5iS4M0VAk01ogAK+RVaHRPUOmq0RirUagf0Ngo0NQk2uUYrVW0h5ox2u6jMt4k2NdMsK+Fzh4pvQN6hkZcDPS56QhYCLjtBH0uOv1uPHYzNqMWj92M02Kgw+vEopVj1SnKpbNDryJgNxF0WPBbDAQdJgJWPXadHKdBideixWPWYNfIMMlbsKml+MxaekrCjuvG1urkEBszYXYXJylE1zhJbnGVi/M8n+AyG+M8E+UwsUF+d4WD+DqvDtOc7e9ylouxv7PE/s4S25EJNuZHGev10+u3MdTpZnlqqLzIf56JsTo5SGp9notsjFeHaT5+dcJXH73k15+/yw+/+oQ//vpTvvvyA37//Vf84rMP+M+//cCf/vgLPv70gM3dHnaTQWZXlExGGhlbeMTI3H3C8/eZWHrI5HI1Y5FqhucqCM9XijFXRXiuirGFJ0xERHjnVhpZ3pKwuiNjPSpjK6EsZ+DrEjq6r2Q3rWA3I2bhvQMbyUM7qRMXmTM/2fNAGeDkkZV43sjOvprtpIztpPgLux5rZWWnpfTOlrCdVJT+k9CUT4fuF03kjqwUTx0cXbg5u/RzfuXj7NLL2ZWb0+cezl54OX3pfWtzKHdSsnstRf5ET+5YR/4a4gMFqYNSKV1QlDPoXk5FMq8un3bJHhnETFvUlgwKdBSODGSPDGQO9aSKWhI5FdGcgt2snGgpC8cLKrHDfSDKOHOHGvJHWgrHOvEIeFFHpqBlL6cSvaOTMlZ3m8sAD88+Fn3Kwg8IDtwm0HsLf89N2rtv0tFzC6Gm6RYtsgrqG28x0GdG2iTQ5W+j3dWA31NHq0RAZbyFI/iUwEAjesddOvubmJjTM71owNFRSd9oC6MTKiZHNdTcE1DWCaxP+vnoKkXbU9HEvanhBlJ5BXJ1JSr9Q+SaezRJxIWH+iaBNtlNnN5a0UjPVUWLQvx3R8b1hHoVBLvUDAw5GQt30tftI+C247aZ8TosmHUqjBoFZp0Kq0GDz2nFYVTjtRnKADsNajEb20y4DRp8Zi1OnaIMsMeswW/T4zWJEFtVEhxaOQGblr6AnbEuHxM9Aca7S3vCQ11sz49zGF/nIhPlKhcXQS7scZmLc56J8rywxwcXRV4dprks7nFZ3KOQWGdvY5612RFmR7rK3e/oygyx1VmKsXVyOytEI5OsTw+T2YrwvLBXBvhffvE+v/36Q7755CUfvjrmq8/f41//8h1/++t3fPfdO7z34R7xdB9TiypGZxvLKqzw/ANGFx6Igo6lKsYi1YQXHjK2KJbRo/PVpa2lGqaWa5lbaWR+takEsISNmPytEjqWVhJLi/DupORsp1XE80YSRSt7B3aSR272T31kzvxkzvzsn7hJHtqJ5Qxsp1VleK8BXt0VYzNR6kBntKXToXrSBSOZAzP5Y3Ed8fjSw/lVgIvnfs6fezgvwXv+0ifKKV94OLiwkT81l+fN/x3g3KFGlFX+BOBEXv4PAGcOjGWAr9+7mUM9hSMDxWNjGezr9cJrcK8jUVSXAU4fqsn+BN7iib7cxU7m1cQz4s9yJdbCwmYjM6u1jMw/YrD8DhYPpwd6bxHsvU1n3x0EmaEGheEprZJ7eN2t6OT3mRm2ELDXMtSrRK4UkKoFzL5HuLvqUZlu4/A/pn9QxsZuB0bXXYanlbh91fR1tdD2VMDQepNwu4JPruJ4jXU0PhFobb5Jm+w2LbJbKA0VtChuUtsiLvY3tgm0ym9icT0l1C/H5n1ES+mwWaCzib4hLWNjToaHXHR12vG5LbhtZpwWI26bCaNGgdWgQa+SYTVosBm12A0qXGYtJqUEm1aBVSPHbzPS6bSW58ABq5iZHXoVLqMGn9WA36YXrypo5djUUlx6JX6Ljr6AnXC3j6FON2O9AVYmBolGpogvTZHdinAQW+M0tc3LgxTvnmR5/yzPp69O+ez1GR8/P+Ld8zyfv3/J1UGS02yU1NZiqaweZrTHz/r0CLmdFU5SO+wsTJTXEpNrc7w+2ufz9y/59rP3+MN3n/Kbbz7k5++d8u7zPN99+xH//tfYzC3LAAANe0lEQVTv+Nt//oof/vQ+yewg61EnEwutDE/XiPajCw8ZXahgdOEBY4uVjEcellYL/zGmlp8ys1rL4noTkY1mVrbbytl3N60ivq8hlhYbWbtJOVt74rttK6UkmhVL6ETRxt6hi/SJl/1TH/unPtLHLvYObESzerZSyjK8mwkZG3Ep6zEJG3Epu0kliYyWVN7AftHEftFE5sBczr4n527OLr1cPPdz9bKdy5c+Ll/6uHjl5/J1gMvXAc5f+ji6clE8t5cEGGbyJ8ZSXAOsEuEtykkW5aXsKyO2LyspqMSlg+tGVbaUbfM/gffgxCR2pQ90oi46q2S3pOy6fgMnimpxD/lQFJFkj7TkSgAfnBrIHRvFv6OgZy8rmgysxltZ3GpiZr2O8MJjhmaryhczro+nhwZu0zV4B+FJ222U5qc0td7k2WMBdcsNtheCLIzaGO5R09oqoLdWoDDfJTgsp3NAislRhcv9jKkZC6MLJrTOu5jsD7CY7jHWp6T2noBTWcFFZo6VSR+yJoHWFoGW0nEyubGCBrlAnVSgtu0GjZJbtChuo7dW4+tsxu6tQaq6vkQo0DOgZmEhSF+vFaO+Fa2qDbNOjVknNq50SikOs74MsF4lw6Bsw6SW4jSoy3PgdoeZDrsZp06FS6/EqpLgMetwm7S4TVq8Fn1ZF91hF6M/4KLDbqTLY2agw1kGeWGkh/nh7rKgY2W8n9WJAaKRSbLbS5ymd3j3JMtnr8/EeO+CX/78HT56ecz7l0UuCgmO93eIr80xF+5hqq+D7PYy+5sRdhcnmRsKsTzeT2YrwnunOb742RXff/kh//bD1/z2lx/xxcdXfPPla/71f3zFb373AV/98pTPv86wsuVgfk3NZKSRichTwnNVjC5UMLZYydhiRfnz5PLjspXLdUyv1jCzWsv8RgNLmy2sbLextitlIy6OdWIZsXEVSyvZTcrZTkjZiLeJ8O0p2c3oiOUtxAtWEgdOUscekkduMQ4dxAsWdjMGtlJqtvbkbCYkbCZkbCZkbO3JxXfvvoZU3lCGNnckGt4VTx0cnrvegvf5qw6evw7w/HWAq3fauXqnnefvdnD5OlAqpd9ALGquTRRODeI7+EBJqihnLy8lnpMQy0rZ3W9jJ9lWUpmJQpVrT6/MoZ7csZHCkaEMcOHU9Fb2jWUUbO9L2cnIyhD/NANfA5wvwXtwaiB/YiJ3LJ5P3ctr2ckoWUu0sbD9NsDXFzM6S9cge4bv0TtyH0Fhr+X+U4HqJwJGbRUOTRUdllo2Z3zMjJhRq2/jbm+gRX0DqfE+rvYmzM4a5LKbdHVLaR9oxd1VR0d3E3KpgFYi4FDfxat7yMaUi4PYKFrZbZoaBVplAi2KG8iND6iTCtTJBKobRYhb1ffQ2x7hCNRhclUjUYvZuVki4PTW099vJOBXY9C1oJI3opK3opC0oFNKkbc2YtIqUUqa0atkKCXimU+TWopdpyx3oP02Iz6zHqtKhk0txSRvwW3S4rMayo2uTreNbp+T/g43QyEfo93t9PocDHZ4GO8NMtrjZ2qwk4WxPuaGuogvTRGLTLI2OcjiSDcrEwNszY2SWJmhGFvj1WGaDy6KfPPJa7766CVff/yKn79zxvuXRQqJdVZnRohMDLCzMEExtk5qdZ6VsQH6PGZikUkSy9McJ7d49zzPd1/8jB//8A2/+9XH/Pqr9/n3H3/JH/78Mb/6zSUH5zNsJwNsJR0sbMkYmK5kcuUJ4fk3WXc8IpbM45Fqppaflh0Rp1cfM7P2hJm1J8xt1LK43cDqbitr0TY24lK29uTsplVlgMunMmNtrEVbWIu2sZ5QsLOvJZozixAXHewdukgcOEkcONk7sBPLm98C+DquS/Prsvn6vVs4sVM8dYjOHWcuTi88b8H74nWwdP/Iz8t3O8TP74W4eifI+et2Tl74OLhwiHFupXhm5vDMSKEEcLIgI5ZpJZppZTfVyvaeeGxsKyEu3l87al4LNgpHbwAuHOvIHRtIl5pXu1kFW2kpm6k2NlNtbGWkbGdl5WZWsqgsl9C5Yx0Hpwbxezk1kzsWbX7+GcCji0/KAHeF7xEavkNo+A494Qf0jlYgtJmqsXe0ojVUI20W0LfdptNSRyISwqS8h1J5E4evFldnCzUtAg0SAYP5EY8qBYIdbaitlbTqBYyOCgb6JCibBdT1Am1VAlszLs72x3Gaq2lrFpAobiLT3kVmuE+tVOCZRKCmVeCZ5AbNyjvIDPdRme4j199CohZolgsoNLfRGitxOBrxeuRYTFJkbbVIWxvQKKRlcLUKCW2NtUib65G1NKCWNGLWyNDLWrCoZZhVUhFWsx6nToXfosNr0mBSSsrZOeiyEnRZ6fI66O9wM9jppcttpdfnYLw3yNxIHzPDXcwMd7Ew1sfieD8782Ok1ubIbkUoRlc52tskv7tCYmWG+PI0+d0VzjNRPnvvgs/eu+D7Lz/k28/e4xcfPOd4f4edpSkiEwOkNxbZWZggHplmqqedPo+ZpdFelsf62FkY5zi5xdcfv+LffviaP3z/Gb/7/lP+/l/f8/3vXvHlrwtcvDPPatTKSlTL5Eoto4tVhBcfMLZYUX73ihBXM7H0iOmVZ8xt1DK7XsPseg1zG0+Z33zG4nYDS7tNrMck5cy4nVSwm1YQ3RcF+ztJCZvxltKpkGYR9ric7bSG3ayJaM5MNG8jXnSUI1G0ES9YiGaN7OxrxS2mUsQymjK415n3Gt5r947TCw/nVz4uXwTegvflu4FSvAH4+budpfXCAIeXbg4vnRxe2Di8KJ3+PNaRKSpI5aRE91vYSTeX4G1kPdrIRkyUi+4kJWWA84cGiscmMfMeGcgdasgcimuB8aySrbSU9b1W1hLNYiRbWE+1lrNxPCcjWVSKjaz/D8Db+wrWEm3MbzUyvVbL6OIThueqywCLR9Tv0TdWycBEFUKN4gaWjiaa2wRkLQImyR282moWhyx02OtoKWXOvnETni4JSsND1LqH3L0pYDJW0yAXqFcIdPZJaPfVMdIjof6+gE0mMOxv5Co/ydigGqPuLlLlDaQaEdQm1S2eSASeyW7xpE2gVirQpLpFk1KgVSUg1wko9DdQau+gNVZitdZhtTShUTXQ1lyDQtqMSadGr5KhaGtC2lxPS/1T6muqkTTVIWt6hlElwaKWYdcpsahloh5aq8SskJTnvkZFGw69Cq9FX254tTvMhDxWun12erx2Bjs8DHf6yr5a/QEXPT5xAWEoYGdusJPtuVGyWxFO0zscJjZIrc+ztzpb7kY/P0zx4YsjvvnkNb/56iN++fN3eH2aJb29xFy4h6WxftEkYKibuYEQO/NjrIz3Mz8UYmmsj/TGAh++OOL3337CX37/JX/43Rf88Q+f8tmXRS5eL3J4GWY7bWduo5XwwkOm1x8zGrnPxEqlGEsPmViqYmLpEZNLYqk8t1HLwlYtC1vPWNiqZXG7jqXdJlbjrSU1lLwcu2kR4msniY1YE2u7jf8U4N2sid2clVjBLsJbysCJopV43kwsZ3ijssoaSBfMZA8t5I9tFE7sFE7sHJw5OTx3cXLu5vTC8w+l84t32nn5bgev3gnw6p1A+c9lgF93cvYq8GaN8dLO4YVVPL59rCNbeAPwbrqZ7WQjG7EG1qMixNcAp7IqMgUthSMjh6fi6dDCkYHsgZp0UcVeTkV0X85Gso2VWBNLu/UsRxtYjjeykmhiI9nKVlpCNCMhkZeTLqrIHmnLb+D8idggSxX1ZafM1Xgrc5sNTK0+YyxSw8j8I/onH9Iz9oCe0fv0jj1gYPIhwzOPEJqN91HYqlFq7tPdIaX1kYChSWBt3MXypAufu56aOgG1tZq+cRNmVx2t0lvUPhLQ66owe2vx9UkwO6sJD6mZHzdglQkEDA/odz/j/bMIsc0gAW8tUqVAq/Imbfq7yIwVPJUJVDUJVDYI1MluITNWIDPcRaK/iVwnINOKGdhsf4LFUotUUkFLUxVqRRMGrQK1XELjs8fUPn7I0+oKGp4+oqbqAdLmetrqn6BXtKJsrkPT1oiqpR6DvBWTvA29pLk8+3Ua1OU3sF2nxKFX4bcZywCPhPyMhPxlT63J/hBTA13lUnrAZyUcdDEW8jA32MnOwjiZrQiHiQ2u8gleFJOcpnfIxVZ5dZLh/UuxnP7svQvevyxymo0RW51laayf3cVJuh1GZvqCHMTWiIR7iIR7WBjpJhqZ5OJgj28+fcWffvsFv//NZ/zii0tOLpbYSQXpH29iZLaBrX0Nc1vP6J++wdz2I6bWK0X3jeUq0dRuqYbplWfMrtWVyuU6FrdrxTs9uw2sxFpY35Owk5KXZ7XX8O6kxOwrHupq/P8CvJO1EM3bRHgPXaSOnCQP7SQPbCQKJpKF0vJ+wUz2UHzj/tTw7vDcxdGFCO8/A/g64756J8DL1/4ywM/f7XwD8MsOjp+Lb+HjSxdHl7a3AE5mJcTTbwDeTNS/lYF3U1L282L5fHBi5ujs+ni3CHCqoCSRVbKbFtVky9FGIjt1YkTrWYo1sJZoZiPZyk66lXhORqqgJHOo+acAx7NqttJiF3pus4HJlaeMLz0lvCCOknrHK+gde0D/RCVD09WE557wfwFytNilM+i4IQAAAABJRU5ErkJggg==); background-size: cover; background-position: center; }
        #themePicker .panel-body .mini-body { font-size: smaller; padding-bottom: 10px;}
        #themePicker .panel-body .mini-body i { font-size: 24px; display: block; padding: 10px 0px; text-align: center;}
        #themePicker .panel-body .foot { font-size: smaller; text-align: center; }
        #themePicker .panel-footer a { color: #41BEA8; float: right; }
      </style>
      <ul id="themePicker" class="list-inline">
        ${themesList(themes)}
      </ul>`
    );

  /** IntroJS ******************************************************* */

  $('head').append(
    '<script src="' + gravRoot + '/user/plugins/customadmin/intro.min.js"></script>'+
    '<link href="' + gravRoot + '/user/plugins/customadmin/introjs.min.css" rel="stylesheet">'
  );

  // Tableau de bord
  if (location.href.indexOf('/admin/') === -1 ) {
    startHelp = function () {
      introJs()
        .setOptions({
          'prevLabel': '← Précédent',
          'nextLabel': 'Suivant →',
          'skipLabel': '&times;',
          'doneLabel': '&times;',
          'scrollTo': 'tooltip',
          'tooltipClass': 'introjs-lg',
          'showBullets': false,
          steps: [
            {
              intro:
                '<h5>Découvrez l’espace d’administration de votre Framasite</h5>'+
                '<p>Voici un guide rapide pour vous présenter l’interface. N’hésitez pas à vous référer à la <a href="https://docs.framasoft.org/fr/grav/">documentation</a> pour une utilisation avancée.</p>'+
                '<p>Lorsqu’une aide contextuelle est disponible comme ici, un bouton <a class="button" style="background-color:#00A6CF"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a> apparaît dans la barre d’entête.</p>'+
                '<p>Cliquez sur les boutons « Suivant » et « Précédent » ci-dessus pour naviguer dans l’aide contextuelle.',
            },
            {
              element: '#admin-menu [href$="config/system"]',
              intro:
                'Normalement le site est déjà correctement configuré mais si vous voulez changer le titre, l’auteur et la description. Ça se passe dans le menu « Configuration > Site ».',
              position: 'right'
            },
            {
              element: '#admin-menu [href$="pages"]',
              intro:
                'Des pages d’exemple vous sont proposées avec votre Framasite pour pouvoir rapidement créer un site fonctionnel. Pour les modifier ou en ajouter des nouvelles, ça se passe dans le menu « Pages »…',
              position: 'right'
            },
            {
              element: '#latest .button',
              intro:
                '… ou en suivant le bouton « Gestion des pages » ci-contre',
              position: 'left'
            },
            {
              element: '#admin-menu [href$="plugins"]',
              intro:
                'Le site est fourni avec plusieurs plugins (dé)sactivables selon vos besoins ainsi qu’un unique thème que vous pouvez personnaliser avec des feuilles de style grâce au plugin CustomCSS.',
              position: 'right'
            },
            {
              element: '#admin-menu [href$="backup-manager"]',
              intro:
                'Par la suite, si vous voulez exporter votre site Grav pour l’héberger ailleurs (les fonctionnalités étant volontairement limitées ici) ou simplement pour en conserver une copie, vous pouvez gérer vos sauvegardes depuis le menu « Backup ».',
              position: 'right'
            }
          ]
        })
        .onbeforechange(function (targetElement) {
          $(targetElement).focus();
        })
        .onexit(function () {
          $('html, body').animate({
            scrollTop: 0
          }, 1);
        })
        .start();

      $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
    }

    $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>');

    // Affichage auto de l’aide contextuelle une seule fois
    if ( !/(?:; )?introjs=([^;]*);?/.test(document.cookie) && $('#admin-main').length) {
      startHelp();
      var expires = new Date();
      expires.setTime(new Date().getTime() + 3153600000000);
      document.cookie = "introjs=true;expires=" + expires.toGMTString();
    }

  }

  // Configuration Site
  if (location.href.indexOf('/config/site') > -1) {
    startHelp = function () {
      introJs()
        .setOptions({
          'prevLabel': '← Précédent',
          'nextLabel': 'Suivant →',
          'skipLabel': '&times;',
          'doneLabel': '&times;',
          'scrollTo': 'tooltip',
          'highlightClass': 'hidden',
          'tooltipClass': 'introjs-lg',
          'showBullets': false,
          steps: [
            {
              element: '[name="data[title]"]',
              intro:
                'Vous pouvez remplacer le titre de votre site. C’est ce titre qui apparait dans les résultats des moteurs de recherche et dans la fenêtre de votre navigateur web.',
              position: 'left'
            },
            {
              element: '[name="data[author][name]"]',
              intro:
                'Vous pouvez changer le nom de l’auteur…',
              position: 'left'
            },
            {
              element: '[name="data[author][email]"]',
              intro:
                '… et l’adresse email correspondante…',
              position: 'left'
            },
            {
              element: '[name="data[metadata][description]"]',
              intro:
                '… ainsi que la description par défaut de votre site. Elle ne sera pas visible sur votre site. Seuls les moteurs de recherche en tiennent compte. Vous pourrez ajouter d’autres métadonnées ici plus tard.',
              position: 'left'
            },
            {
              element: '#titlebar button[type="submit"]',
              intro:
                'N’oubliez pas d’enregistrer vos changements.',
              position: 'left'
            }
          ]
        })
        .onbeforechange(function (targetElement) {
          $(targetElement).focus();
        })
        .onexit(function () {
          $('html, body').animate({
            scrollTop: 0
          }, 1);
        })
        .start();

      $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
    }

    $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>')

  }

  // Gestion des pages
  if (location.href === gravRoot + '/admin/pages') {
    startHelp = function () {
      introJs()
        .setOptions({
          'prevLabel': '← Précédent',
          'nextLabel': 'Suivant →',
          'skipLabel': '&times;',
          'doneLabel': '&times;',
          'scrollTo': 'tooltip',
          'tooltipClass': 'introjs-lg',
          'showBullets': false,
          steps: [
            {
              intro:
                'Pour vous aider dans la construction de votre site nous l’avons prérempli avec quelques pages d’exemples.<br>'+
                'Une icône « maison » <i class="fa fa-home" style="color:#8d959a"></i> indique quelle est la page utilisée comme page d’accueil. Vous pouvez en changer dans la partie <a href="./config/system">Configuration</a>.<br>'+
                'Les pages constituées de sous-pages (comme le blog) ou de sous-modules (comme la page carrousel) sont précédées de cette icône <i class="page-icon fa fa-plus-circle" style="color:#0082BA"></i>.',
              position: 'bottom-middle-aligned'
            },
            {
              element: '[data-nav-id="/contact"]',
              intro:
                '<img class="col-sm-6" src="../user/plugins/customadmin/images/th_contact.jpg" alt="" />'+
                '<p class="col-sm-6">Exemple de formulaire de contact. Pour qu’il fonctione, il faut définir les adresses emails de l’expéditeur et du destinataire dans le plugin « email ».<br>Pour modifier le texte du formulaire et notamment le captcha, il faut passer en mode d’édition expert.</p>'+
                '<p class="col-sm-12"><a href="../contact" class="btn btn-xs btn-primary">Voir</a> '+
                '<a href="../admin/plugins/email" class="btn btn-xs btn-warning">Configurer</a> <a href="./pages/contact/mode:expert" class="btn btn-xs btn-info">Modifier (mode expert)</a></p>',
              position: 'top'
            },
            {
              element: '[data-nav-id="/common"]',
              intro:
                'C’est dans ce fichier que vous pouvez modifier la barre de navigation et le pied de page du site.'+
                '<p class="col-sm-12"><a href="./pages/common/mode:normal" class="btn btn-xs btn-info">Modifier</a></p>',
              position: 'top'
            },
            {
              element: '[data-nav-id="/images"]',
              intro:
                'Ce dossier contient l’ensemble des images communes à tout le site. Il sert notament de référence pour le module <code>images-collage</code>.',
              position: 'top'
            },
            {
              element: '#titlebar .button-group button[data-remodal-target="modal"]',
              intro:
                'Cliquez sur ce bouton pour ajouter une nouvelle page ou un nouveau dossier. Remplissez ensuite le formulaire en précisant bien un « Template de page » correspondant à l’exemple qui vous convient parmi ceux proposés ici en démo.<br>Pour ajouter un article dans la section blog, il faut choisir le template <code>item</code> et <code>blog</code> comme « Page parente » (ou tout autre page utilisant le template <code>blog</code>).',
              position: 'left'
            }
          ]
        })
        .onbeforechange( function (targetElement) {
          $(targetElement).focus();
        })
        .onexit( function () {
          $('html, body').animate({
            scrollTop: 0
          }, 1);
        })
        .start();

      $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
    }

    $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>');

    // Bugfix : quand les dossiers 'images' et 'commons' sont modifiés, une page est autamatiquemunt créée sans titre ce qui empèche toute modification ultérieure
    // Ce bout de script récupère la fin de l’url d’édition pour l’ajouter comme ancre
    jQuery('a.page-edit').each(function () {
      if (jQuery(this).text() === '') {
        jQuery(this).text(
          jQuery(this).attr('href')
            .replace('/admin/pages/','')
            .replace(gravRoot, '')
            .replace(/\b\w/g, function (l) { return l.toUpperCase() })
        )
      }
    });

  }

  // Édition ou ajout d’une page
  if (window.location.href.indexOf('/admin/pages/') > -1 &&
    window.location.href.indexOf('mode:expert') === -1 ) {
    startHelp = function () {
      introJs()
        .setOptions({
          'prevLabel': '← Précédent',
          'nextLabel': 'Suivant →',
          'skipLabel': '&times;',
          'doneLabel': '&times;',
          'scrollTo': 'tooltip',
          'tooltipClass': 'introjs-lg',
          'showBullets': false,
          steps: [
            {
              element: '[name="data[header][title]"]',
              intro:
                'Saisissez le titre de votre article',
            },
            {
              element: '.grav-editor-content .CodeMirror',
              intro:
                'La rédaction des articles se fait en utilisant <a href="https://docs.framasoft.org/fr/grav/markdown.html">la syntaxe markdown</a>, mais vous avez également à disposition une barre d’outil pour vous aider. Il est aussi possible d’ajouter des modules et composants Bootstrap à l’aide de codes raccourcis. Pour en savoir plus, veuillez consulter <a href="https://docs.framasoft.org/fr/grav/">la documentation</a>.',
              position: 'left'
            },
            {
              element: '.grav-editor-button-preview',
              intro:
                'Pour avoir un aperçu de votre texte tel qu’il sera publié, cliquez sur ce bouton <i class="fa fa-eye"></i>. Pour revenir au mode édition, cliquez sur cette icône <i class="fa fa-code"></i>.',
            },
            {
              element: 'tab-content.options.advanced1 .form-field',
              intro:
                'Les images associées à l’article ne peuvent être ajoutée qu’une fois l’article enregistré.<br>Sur les articles de blog, la première image sera mise à la une.<br>Pour ajouter une image dans le corps du texte, passez simplement la souris sur l’image et cliquez sur le bouton <i class="fa fa-plus-circle"></i> à sa droite.<br>Un code markdown de ce genre devrait être inséré : <code>![](une-image.jpg)</code>.<br>Les images contenues dans le dossier <code>images</code> (dossier qui se trouve dans le menu « Pages » ) s’ajoute manuellement avec ce code : <code>![](/images/une-autre-image.jpg)</code>.',
            },
            {
              element: '[data-tabid="tab-content.options.advanced2"]',
              intro:
                'Dans les « Options » vous pouvez définir la catégorie et les mots-clé de l’article ainsi que les paramètres de publication (il est possible de la planifier à l’avance).',
            },
            {
              element: '[data-tabid="tab-content.options.advanced3"]',
              intro:
                'L’onglet « Avancé » permet de configurer comment on accède à la page dans l’arborescence du site. S’il s’agit d’une page présente dans la barre de navigation, vous pouvez par exemple définir sa position dans le menu.',
            },
            {
              element: '#titlebar button[type="submit"]',
              intro:
                'N’oubliez pas d’enregistrer vos changements une fois terminé.',
            },
          ]
        })
        .onbeforechange(function (targetElement) {
          $(targetElement).focus();
        })
        .onexit(function () {
          $('html, body').animate({
            scrollTop: 0
          }, 1);
        })
        .start();

      $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
    }

    $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>')

  }

  if (window.location.href.indexOf('/admin/tools') > -1 ||
    window.location.href.indexOf('/admin/themes') > -1) {
    window.location.href = '/admin';
  }
});
